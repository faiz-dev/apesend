const express = require("express");
const path = require("path");
const app = express();
const cors = require("cors");
const https = require("https");
const fs = require("fs");
const { MongoClient } = require("mongodb");
const url = "mongodb://localhost:27017/";
const client = new MongoClient(url, { useUnifiedTopology: true });
let db;

app.use(express.json());
app.use(cors());

app.get("/centiment", async (req, res) => {
  let stepSize = 0,
    votesDocumentsToSearch,
    sortable = [],
    bestTickers = [],
    { unit, step, ticker } = req.query;
  let noOfTickersToShow = 10;
  let regex;
  switch (unit) {
    case "hourly":
      stepSize = 24;
      votesDocumentsToSearch = 1;
      regex = / G.*/g;
      break;
    case "daily":
      stepSize = 720;
      votesDocumentsToSearch = 24;
      regex = / [\d]{2}:.*/g;
    case "weekly":
      stepSize = 720;
      votesDocumentsToSearch = 168;
      regex = / [\d]{2}:.*/g;
  }

  let votesCollection = await db.collection("votes");
  let votesResult = await votesCollection
    .find({})
    .sort({ _id: -1 })
    .limit(votesDocumentsToSearch)
    .toArray();
  let voteTickers = {};
  votesResult.forEach((doc) => {
    delete doc["_id"];
    for (let tic in doc) {
      if (!Object.keys(voteTickers).includes(tic)) voteTickers[tic] = 0;
      voteTickers[tic] += doc[tic][4];
    }
  });
  for (let x in voteTickers) sortable.push([x, voteTickers[x]]);
  sortable.sort((a, b) => b[1] - a[1]);
  if (sortable.length < noOfTickersToShow) noOfTickersToShow = sortable.length;
  for (let x = 0; x < noOfTickersToShow; x++) bestTickers.push(sortable[x][0]);

  const collection = await db.collection("sentimental");
  const {
    0: { incremental: max },
  } = await collection.find({}).sort({ incremental: -1 }).limit(1).toArray();
  const lte = max - stepSize * (step - 1),
    gte = lte - stepSize + 1;
  const results = await collection
    .find({
      $and: [{ incremental: { $lte: lte } }, { incremental: { $gte: gte } }],
    })
    .sort({ incremental: -1 })
    .toArray();
  results.forEach((doc) => {
    delete doc["_id"];
    delete doc["incremental"];
    doc.Date = doc.Date.toString().replace(regex, "");
  });
  const distinctDates = [...new Set(results.map((doc) => doc.Date))];
  const distinctWeeks = ["Week 1", "Week 2", "Week 3", "Week 4"];

  if (!ticker) {
    let respose = {};
    bestTickers.forEach((tic) => (respose[tic] = []));
    if (unit === "hourly") {
      results.forEach((doc) =>
        bestTickers.forEach((tic) =>
          respose[tic].push({
            date: /*new Date(doc['Date'])*/ doc["Date"],
            value: doc[tic] ? doc[tic] : 0,
          })
        )
      );
    } else if (unit === "daily") {
      bestTickers.forEach((tic) => {
        distinctDates.forEach((date) => {
          const matchedDocs = results.filter((doc) => doc.Date === date);
          let datePerformance = { date /*: new Date(date)*/, value: 0 };
          matchedDocs.forEach((mdoc) => {
            datePerformance.value += mdoc[tic] ? mdoc[tic] : 0;
          });
          datePerformance.value /= matchedDocs.length;
          respose[tic].push(datePerformance);
        });
      });
    } else {
      results.forEach((doc, index) => {
        if (index < 168) doc.week = "Week 1";
        else if (index < 336) doc.week = "Week 2";
        else if (index < 504) doc.week = "Week 3";
        else doc.week = "Week 4";
      });
      bestTickers.forEach((tic) => {
        distinctWeeks.forEach((week) => {
          const matchedDocs = results.filter((doc) => doc.week === week);
          let weekPerformance = { date: week, value: 0 };
          matchedDocs.forEach((mdoc) => {
            weekPerformance.value += mdoc[tic] ? mdoc[tic] : 0;
          });
          weekPerformance.value /= matchedDocs.length ? matchedDocs.length : 1;
          respose[tic].push(weekPerformance);
        });
      });
    }
    res.send(respose);
  } else {
    let respose = { [ticker]: [] };
    if (unit === "hourly")
      results.forEach((doc) =>
        respose[ticker].push({
          date: /* new Date(doc['Date'])*/ doc["Date"],
          value: doc[ticker] ? doc[ticker] : 0,
        })
      );
    else if (unit === "daily") {
      distinctDates.forEach((date) => {
        const matchedDocs = results.filter((doc) => doc.Date === date);
        let datePerformance = { date /* : new Date(date)*/, value: 0 };
        matchedDocs.forEach((mdoc) => {
          datePerformance.value += mdoc[ticker] ? mdoc[ticker] : 0;
        });
        datePerformance.value /= matchedDocs.length;
        respose[ticker].push(datePerformance);
      });
    } else {
      results.forEach((doc, index) => {
        if (index < 168) doc.week = "Week 1";
        else if (index < 336) doc.week = "Week 2";
        else if (index < 504) doc.week = "Week 3";
        else doc.week = "Week 4";
      });
      distinctWeeks.forEach((week) => {
        const matchedDocs = results.filter((doc) => doc.week === week);
        let weekPerformance = { date: week, value: 0 };
        matchedDocs.forEach((mdoc) => {
          weekPerformance.value += mdoc[ticker] ? mdoc[ticker] : 0;
        });
        weekPerformance.value /= matchedDocs.length ? matchedDocs.length : 1;
        respose[ticker].push(weekPerformance);
      });
    }
    res.send(respose);
  }
});

app.get("/donut", async (req, res) => {
  let sortable = [],
    response = [];
  let noOfTickersToShow = 5;
  const collection = await db.collection("tickerCounter");
  const {
    0: { tickers },
  } = await collection.find({}).sort({ _id: -1 }).limit(1).toArray();
  for (let tic in tickers) sortable.push([tic, tickers[tic]]);
  sortable.sort((a, b) => b[1] - a[1]);
  if (sortable.length < 5) noOfTickersToShow = sortable.length;
  for (let i = 0; i < noOfTickersToShow; i++) {
    const [tic, value] = sortable[i];
    response.push({ [tic]: value });
  }
  res.send(response);
});

app.get("/table", async (req, res) => {
  const noOfTickersToShow = 10;
  let { ticker, unit } = req.query;
  let stepSize;
  switch (unit) {
    case "hourly":
      stepSize = 1;
      break;
    case "daily":
      stepSize = 24;
      break;
    case "weekly":
      stepSize = 168;
      break;
    default:
      stepSize = 7;
  }
  const collection = await db.collection("sentimental");
  const {
    0: { incremental: max },
  } = await collection.find({}).sort({ incremental: -1 }).limit(1).toArray();
  const gte = max - stepSize + 1;
  const results = await collection
    .find({
      $and: [{ incremental: { $lte: max } }, { incremental: { $gte: gte } }],
    })
    .sort({ incremental: -1 })
    .toArray();

  if (!ticker) {
    const firstDoc = results[0];
    let respose = {},
      bestTickers = [],
      sortable = [];
    results.forEach((doc) => {
      delete doc["_id"];
      delete doc["incremental"];
      delete doc["Date"];
    });
    for (let tic in firstDoc) sortable.push([tic, firstDoc[tic]]);
    sortable.sort((a, b) => b[1] - a[1]);
    for (let x = 0; x < noOfTickersToShow; x++)
      bestTickers.push(sortable[x][0]);
    bestTickers.forEach((tic) => (respose[tic] = []));
    results.forEach((doc) =>
      bestTickers.forEach((tic) => respose[tic].push(doc[tic] ? doc[tic] : 0))
    );
    for (let tic in respose) {
      respose[tic] =
        respose[tic].reduce((total, num) => total + num) / stepSize;
    }

    res.send(respose);
  } else {
    let respose = { [ticker]: [] };
    results.forEach((doc) =>
      respose[ticker].push(doc[ticker] ? doc[ticker] : 0)
    );
    for (let tic in respose) {
      respose[tic] =
        respose[tic].reduce((total, num) => total + num) / stepSize;
    }
    res.send(respose);
  }
});

app.get("/bar", async (req, res) => {
  let { unit, ticker } = req.query;
  let sum = 0,
    tickerOccur = 0;
  let noOfDocuments;
  switch (unit) {
    case "hourly":
      noOfDocuments = 1;
      break;
    case "daily":
      noOfDocuments = 24;
      break;
    case "weekly":
      noOfDocuments = 168;
      break;
    case "monthly":
      noOfDocuments = 672;
      break;
    default:
      noOfDocuments = 7;
  }

  const scorePromise = new Promise(async (resolve, reject) => {
    try {
      let collection = await db.collection("sentimental");
      const {
        0: { incremental: max },
      } = await collection
        .find({})
        .sort({ incremental: -1 })
        .limit(1)
        .toArray();
      const docsForScore = await collection
        .find({
          $and: [
            { incremental: { $lte: max } },
            { incremental: { $gte: max - noOfDocuments + 1 } },
          ],
        })
        .sort({ incremental: -1 })
        .toArray();
      docsForScore.forEach((doc) => {
        if (Object.keys(doc).includes(ticker)) sum += doc[ticker];
      });
      resolve();
    } catch (ex) {
      reject();
    }
  });

  const countPromise = new Promise(async (resolve, reject) => {
    try {
      let collection = db.collection("tickerCounter");
      const docsForCount = await collection
        .find()
        .limit(noOfDocuments)
        .toArray();
      docsForCount.forEach(({ tickers: tic }) => {
        if (Object.keys(tic).includes(ticker)) tickerOccur += tic[ticker];
        resolve();
      });
    } catch (ex) {
      reject();
    }
  });

  Promise.allSettled([scorePromise, countPromise])
    .then(() => {
      res.send({
        occurence: tickerOccur,
        average: sum == 0 ? 0 : sum / tickerOccur,
      });
    })
    .catch((ex) => {
      res.send({
        message: "Something went wrong",
      });
    });
});

app.get("/votes", async (req, res) => {
  const { ticker, unit } = req.query;
  let noOfTickerToUse = 5;
  let noOfDocuments,
    noOfDocsForTickers,
    regex,
    sortable = [],
    tickerToUse = [],
    response = {};
  switch (unit) {
    case "hourly":
      noOfDocuments = 24;
      noOfDocsForTickers = 1;
      regex = / G.*/g;
      break;
    case "daily":
      noOfDocuments = 720;
      noOfDocsForTickers = 24;
      regex = / [\d]{2}:.*/g;
    case "weekly":
      noOfDocuments = 720;
      noOfDocsForTickers = 168;
      regex = / [\d]{2}:.*/g;
  }
  const collection = await db.collection("votes");

  let votesResult = await collection
    .find({})
    .sort({ _id: -1 })
    .limit(noOfDocsForTickers)
    .toArray();
  let voteTickers = {};
  votesResult.forEach((doc) => {
    delete doc["_id"];
    for (let tic in doc) {
      if (!Object.keys(voteTickers).includes(tic)) voteTickers[tic] = 0;
      voteTickers[tic] += doc[tic][4];
    }
  });
  for (let x in voteTickers) sortable.push([x, voteTickers[x]]);
  sortable.sort((a, b) => b[1] - a[1]);
  if (sortable.length < noOfTickerToUse) noOfTickerToUse = sortable.length;
  for (let x = 0; x < noOfTickerToUse; x++) {
    tickerToUse.push(sortable[x][0]);
    response[sortable[x][0]] = [];
  }

  const result = await collection
    .find()
    .sort({ _id: -1 })
    .limit(noOfDocuments)
    .toArray();
  result.forEach((doc) => {
    delete doc["_id"];
    for (let x in doc) {
      doc.Date = doc[x][5].toString().replace(regex, "");
      break;
    }
  });
  const distinctDates = [...new Set(result.map((doc) => doc.Date))];
  const distinctWeeks = ["Week 1", "Week 2", "Week 3", "Week 4"];
  if (!ticker) {
    if (unit === "hourly") {
      result.forEach((doc) => {
        for (let tic in doc) {
          if (tickerToUse.includes(tic)) {
            response[tic].push({
              upVotes: doc[tic][0] != null ? doc[tic][0] : 0,
              downVotes: doc[tic][1] != null ? doc[tic][1] : 0,
              awards: doc[tic][2] != null ? doc[tic][2] : 0,
              occurInDoc: doc[tic][4] != null ? doc[tic][4] : 0,
              /*date: new Date(doc[tic][5])*/
              comments: doc[tic][3],
            });
          }
        }
      });
    } else if (unit === "daily") {
      tickerToUse.forEach((tic) => {
        distinctDates.forEach((date) => {
          const matchedDocs = result.filter((doc) => doc.Date === date);
          let datePerformance = {
            date,
            upVotes: 0,
            downVotes: 0,
            occurence: 0,
            occurInDoc: 0,
            awards: 0,
            comments: [],
          };
          matchedDocs.forEach((mdoc) => {
            if (mdoc[tic] != undefined) {
              datePerformance.upVotes += mdoc[tic][0];
              datePerformance.downVotes += mdoc[tic][1];
              datePerformance.awards += mdoc[tic][2];
              datePerformance.occurInDoc += mdoc[tic][4];
              datePerformance.comments.push(mdoc[tic][3]);
              datePerformance.occurence++;
            }
          });
          response[tic].push(datePerformance);
        });
      });
      response = filterDateResults(response);
    } else {
      result.forEach((doc, index) => {
        if (index < 168) doc.week = "Week 1";
        else if (index < 336) doc.week = "Week 2";
        else if (index < 504) doc.week = "Week 3";
        else doc.week = "Week 4";
      });
      tickerToUse.forEach((tic) => {
        distinctWeeks.forEach((week) => {
          const matchedDocs = result.filter((doc) => doc.week === week);
          let datePerformance = {
            date: week,
            upVotes: 0,
            downVotes: 0,
            occurence: 0,
            occurInDoc: 0,
            awards: 0,
            comments: [],
          };
          matchedDocs.forEach((mdoc) => {
            if (mdoc[tic] != undefined) {
              datePerformance.upVotes += mdoc[tic][0];
              datePerformance.downVotes += mdoc[tic][1];
              datePerformance.awards += mdoc[tic][2];
              datePerformance.occurInDoc += mdoc[tic][4];
              datePerformance.comments.push(mdoc[tic][3]);
              datePerformance.occurence++;
            }
          });
          response[tic].push(datePerformance);
        });
      });
    }
    res.send(response);
  } else {
    response = {};
    response[ticker] = [];
    if (unit == "hourly") {
      result.forEach((doc) => {
        if (Object.keys(doc).includes(ticker)) {
          response[ticker].push({
            upVotes: doc[ticker][0] != null ? doc[ticker][0] : 0,
            downVotes: doc[ticker][1] != null ? doc[ticker][1] : 0,
            awards: doc[ticker][2] != null ? doc[ticker][2] : 0,
            occurInDoc: doc[ticker][4] != null ? doc[ticker][4] : 0,
            /* date: new Date(doc[ticker][5])*/
            comments: doc[ticker][3],
          });
        }
      });
    } else if (unit == "daily") {
      distinctDates.forEach((date) => {
        const matchedDocs = result.filter((doc) => doc.Date === date);
        let datePerformance = {
          date,
          upVotes: 0,
          downVotes: 0,
          occurence: 0,
          occurInDoc: 0,
          awards: 0,
          comments: [],
        };
        matchedDocs.forEach((mdoc) => {
          if (mdoc[ticker] != undefined) {
            datePerformance.upVotes += mdoc[ticker][0];
            datePerformance.downVotes += mdoc[ticker][1];
            datePerformance.awards += mdoc[ticker][2];
            datePerformance.occurInDoc += mdoc[ticker][4];
            datePerformance.comments.push(mdoc[ticker][3]);
            datePerformance.occurence++;
          }
        });
        response[ticker].push(datePerformance);
      });
      response = filterDateResults(response);
    } else {
      result.forEach((doc, index) => {
        if (index < 168) doc.week = "Week 1";
        else if (index < 336) doc.week = "Week 2";
        else if (index < 504) doc.week = "Week 3";
        else doc.week = "Week 4";
      });
      distinctWeeks.forEach((week) => {
        const matchedDocs = result.filter((doc) => doc.week === week);
        let datePerformance = {
          date: week,
          upVotes: 0,
          downVotes: 0,
          occurence: 0,
          occurInDoc: 0,
          awards: 0,
          comments: [],
        };
        matchedDocs.forEach((mdoc) => {
          if (mdoc[ticker] != undefined) {
            datePerformance.upVotes += mdoc[ticker][0];
            datePerformance.downVotes += mdoc[ticker][1];
            datePerformance.awards += mdoc[ticker][2];
            datePerformance.occurInDoc += mdoc[ticker][4];
            datePerformance.comments.push(mdoc[ticker][3]);
            datePerformance.occurence++;
          }
        });
        response[ticker].push(datePerformance);
      });
    }
    res.send(response);
  }
});

app.post("/add_ticker", async (req, res) => {
  const { Symbol, Name_1, Exchange, Country, Sector, Industry } = req.body;
  const collection = await db.collection("tickers");
  try {
    await collection.insertOne({
      _id: Symbol,
      Name_1,
      Exchange,
      Country,
      Sector,
      Industry,
    });
    res.send({ inserted: 1 });
  } catch (ex) {
    res.send({ inserted: 0 });
  }
});

app.post("/delete_ticker", async (req, res) => {
  const { Symbol } = req.body;
  const collection = await db.collection("tickers");
  try {
    await collection.deleteOne({ _id: Symbol });
    res.send({ deleted: 1 });
  } catch (ex) {
    res.send({ deleted: 0 });
  }
});

app.get("/search/:Symbol", async (req, res) => {
  const { Symbol, Name_1 } = req.body;
  const collection = await db.collection("tickers");

  var regex = new RegExp(req.params.Symbol, "i");
  const results = await collection
    .find({ $or: [{ "Name 1": regex }, { Symbol: regex }] })
    .toArray();
  res.status(200).json(results);
});

function filterDateResults(response) {
  for (let ticker in response) {
    response[ticker] = response[ticker]
      .map(
        ({
          date,
          upVotes,
          downVotes,
          occurence,
          occurInDoc,
          awards,
          comments,
        }) => {
          if (date !== undefined)
            return {
              date: new Date(date),
              upVotes,
              downVotes,

              occurence,
              occurInDoc,
              awards,
              comments,
            };
        }
      )
      .filter((obj) => obj != undefined);
  }
  return response;
}

https
  .createServer(
    {
      key: fs.readFileSync("./privkey.pem"),
      cert: fs.readFileSync("./fullchain.pem"),
    },
    app
  )
  .listen(8080, async () => {
    console.log("Server Started");
    await client.connect();
    db = client.db("Stock2021ape");
    console.log("Connected to the Database");
  });
