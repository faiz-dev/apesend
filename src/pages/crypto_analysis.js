import React, { useState, useContext, useEffect } from "react";
import { useParams, useHistory } from "react-router-dom";
import { TagCloud } from "react-tagcloud";
import { Helmet } from "react-helmet";

import CryptoGraph from "./CryptoGraph";
import CryptoBarGraph from "./Graph/CryptoBar";
import CryptoAreaChart from "./Graph/CryptoArea";
// import Donut from "./Graph/Donut";
// import Histogram from "./Graph/histogram";
import { fetchCryptoData, searchCrypto } from "../utils";
import UnitContext from "../context/context";
import UnitSelector from "../components/common/unitSelector";
import MobileNavbar from "./../components/MobileNavbar/MobileNavbar";

const CryptoAnalysis = () => {
  const { state } = useContext(UnitContext);
  const params = useParams();
  const ticker = params.ticker && params.ticker.toUpperCase();
  const history = useHistory();
  const [enteredTicker, setEnteredTicker] = useState("");
  const [finalTicker, setFinalticker] = useState();
  const [words, setWords] = useState([]);
  const [tickerOptions, setTickerOptions] = useState(null);

  const onSearchingTicker = async () => {
    try {
      const res = await searchCrypto(enteredTicker);
      const { data } = res;
      setTickerOptions(data.slice(0, 5));
    } catch (error) {
      console.log("error on searching");
    }
  };

  const onSearch = async (e, entTckr) => {
    if (e) {
      e.preventDefault();
    }
    setFinalticker(entTckr || enteredTicker.toUpperCase());
    try {
      const res = await fetchCryptoData(
        state.unit,
        entTckr || enteredTicker.toUpperCase()
      );
      const data = res.data;
      console.log(data);
      if (data === {}) {
        history.push("/404");
      }
      const wordsArray = [];
      Object.values(data[entTckr || enteredTicker.toUpperCase()]).forEach(
        ({ comments }) => {
          comments.forEach((wrd) => {
            wordsArray.push({
              value: Array.isArray(wrd)
                ? wrd[Math.floor(Math.random() * wrd.length)]
                : wrd,
              count: Number(Math.random() * 100).toFixed(0),
            });
          });
        }
      );
      setWords(wordsArray.slice(0, 16));
      setTickerOptions(null);
    } catch (err) {
      console.log(err);
      history.push("/404");
    }
  };

  useEffect(() => {
    if (
      enteredTicker === "" ||
      (!/[a-z]/.test(enteredTicker) && /[A-Z]/.test(enteredTicker))
    ) {
      setTickerOptions(null);
    } else {
      onSearchingTicker();
    }
  }, [enteredTicker]);

  useEffect(() => {
    if (ticker || enteredTicker) {
      onSearch(null, ticker || enteredTicker);
    }
  }, [ticker, state.unit]);

  return (
    <>
      {ticker && (
        <Helmet>
          <title>TradeApe - Trade better with AI Sentiment Analysis</title>
          <link rel="canonical" href="https://www.tradeape.co" />
          <meta
            name="keywords"
            content={
              "Tradeape, trading, trade, stock, stock market, bull, bear, candlestick, machine learning, data science, algotrading, nlp"
            }
          />
          <meta
            name="description"
            content="TradeApe uses data science and artificial intelligence to understand market sentiments and forecast trends so that you can make better trading decisions. "
          />
          <meta
            property="og:title"
            content="TradeApe - Trade better with AI Sentiment Analysis"
          />
          <meta property="og:url" content="https://www.tradeape.co" />
          <meta property="og:type" content="website" />
          <meta property="og:site_name" content="TradeApe" />
          <meta
            property="og:description"
            content="TradeApe - Trade better with AI Sentiment Analysis"
          />
          <meta
            property="og:image"
            content="https://drive.google.com/file/d/1tC1VlsvmgkOfahexN6lc4xw_dPpOOVZ6/view?usp=sharing "
          />
          <meta name="robots" content="index,follow" />
        </Helmet>
      )}
      <div className="main-wrapper" onClick={() => setTickerOptions(null)}>
        <div className="page-wrapper">
          <nav className="navbar">
            <MobileNavbar />
          </nav>
          <br />
          <div className="container pt-3">
            {history.location.pathname === "/crypto_analysis" && (
              <div className="d-flex justify-content-around mt-5 flex-wrap">
                {finalTicker && <UnitSelector />}
                <div className="d-flex justify-content-center">
                  <div className="autocomplete" style={{ width: "200px" }}>
                    <input
                      autoComplete="off"
                      type="text"
                      name="ticker"
                      className="form-control"
                      value={enteredTicker}
                      placeholder="Enter Crypto name"
                      onChange={(e) => setEnteredTicker(e.target.value)}
                      id="myInput"
                    />

                    <div
                      id="myInputautocomplete-list("
                      className="autocomplete-items"
                    >
                      {tickerOptions?.map((ticker) => (
                        <div
                          onClick={(e) => {
                            onSearch(e, ticker.Symbol);
                            setEnteredTicker(ticker.Symbol);
                            setTickerOptions(null);
                          }}
                          key={ticker._id}
                        >
                          ${ticker.Symbol} ({ticker["Name 1"]})
                        </div>
                      ))}
                    </div>
                  </div>

                  <button className="btn btn-primary ml-1" onClick={onSearch}>
                    Search
                  </button>
                </div>
              </div>
            )}
            {(ticker || finalTicker) && (
              <>
                <div className="mb-5 mt-5">
                  <div className="card">
                    <div className="card-body">
                      <CryptoGraph
                        ticker={ticker ? ticker : finalTicker}
                        width="1200"
                      />
                    </div>
                  </div>
                </div>

                <div className="row mt-3">
                  <div className="col-xl-6 grid-margin stretch-card">
                    <div className="card">
                      <div className="card-body">
                        <h6 className="card-title"> AWARDS MENTION </h6>
                        <CryptoBarGraph
                          type="dateAwards"
                          ticker={ticker ? ticker : finalTicker}
                        />
                      </div>
                    </div>
                  </div>
                  <div className="col-xl-6 grid-margin stretch-card">
                    <div className="card">
                      <div className="card-body">
                        <h6 className="card-title"> NUMBER OF MENTIONS</h6>
                        <CryptoBarGraph
                          type="dateMention"
                          ticker={ticker ? ticker : finalTicker}
                        />
                      </div>
                    </div>
                  </div>
                </div>

                <div className="row mt-3">
                  <div className="col-xl-6 grid-margin stretch-card">
                    <div className="card">
                      <div className="card-body">
                        <h6 className="card-title"> UPVOTED AND DOWNVOTES </h6>
                        <CryptoAreaChart
                          ticker={ticker ? ticker : finalTicker}
                          width={700}
                        />
                      </div>
                    </div>
                  </div>
                </div>

                <div className="mb-5">
                  <div className="card">
                    <div className="card-body">
                      <TagCloud minSize={12} maxSize={20} tags={words} />
                    </div>
                  </div>
                </div>
              </>
            )}
          </div>
        </div>
      </div>
    </>
  );
};

export default CryptoAnalysis;
