/* eslint-disable no-unused-vars */
import React, { useState, useEffect, useCallback } from "react";
import { Helmet } from "react-helmet";
import { Link, useHistory } from "react-router-dom";
import axios from "axios";
import moment from 'moment';

import Graph from "./Graph";
import Donut from "./Graph/Donut";
import Area from "./Graph/Area";
import Bar from "./Graph/Bar";
import { searchTicker } from "../utils";
import ProjectsTable from "../components/common/projectsTable/projectsTable";
import twitter from "../images/icon-twitter-white.svg";
import reddit from "../images/icon-reddit-white.svg";
import linkedin from "../images/icon-linkedin-white.svg";
import UnitSelector from "../components/common/unitSelector";
import { Menu } from "react-feather";
import MobileNavbar from "../components/MobileNavbar/MobileNavbar";

function Prediction () {
  const history = useHistory();
  const [tickerOptions, setTickerOptions] = useState(null);
  const [enteredTicker, setEnteredTicker] = useState("");
  
  const [dataSS, dataSsSet] = useState();
  const [dataSB, dataSBSet] = useState();
  const [dataSSS, dataSSSSet] = useState();
  const [dataSSB, dataSSBSet] = useState();
  const [dataSP, dataSPSet] = useState();
  const [dataSVP, dataSVPSet] = useState();

  const [structuredDataforSS,setStructuredDataforSS]=useState();
  const [structuredDataforSB,setStructuredDataforSB]=useState();
  const [structuredDataforSSS,setStructuredDataforSSS]=useState();
  const [structuredDataforSSB,setStructuredDataforSSB]=useState();
  const [structuredDataforSP,setStructuredDataforSP]=useState();
  const [structuredDataforSVP,setStructuredDataforSVP]=useState();

  
  const onSearchingTicker = async () => {

    try {
      const res = await searchTicker(enteredTicker);
      const { data } = res;
      setTickerOptions(data.slice(0, 5));
    } catch (error) {
      console.log("error on searching");
    }
  };

const fetchMyAPI = useCallback(async () => {
    let response = await axios('https://tradeape.co:8080/cs')
    let response1 = await axios('https://tradeape.co:8080/cb')
    let response2 = await axios('https://tradeape.co:8080/css')
    let response3 = await axios('https://tradeape.co:8080/csb')
    let response4 = await axios('https://tradeape.co:8080/cp')
    let response5 = await axios('https://tradeape.co:8080/cvp')

    dataSsSet(response.data);
    dataSBSet(response1.data)
    dataSSSSet(response2.data);
    dataSSBSet(response3.data);
    dataSPSet(response4.data);
    dataSVPSet(response5.data);
  }, [])

  useEffect(() => {
    fetchMyAPI();

    if (
      enteredTicker === "" ||
      (!/[a-z]/.test(enteredTicker) && /[A-Z]/.test(enteredTicker))
    ) {
      setTickerOptions(null);
    } else {
      onSearchingTicker();
    }
  }, [enteredTicker]);

  useEffect(() => {
    if (dataSS || dataSB || dataSSS || dataSSB || dataSP || dataSVP) {

      dataSS&& dataSS.forEach(object => {
         let newArray=[];
       for (const key in object) {
        if (key !== "_id") {

          newArray.push({name:key,data0:object[key][0],data1:object[key][1],data2:object[key][2],data3:object[key][3]})
        }
      }
        setStructuredDataforSS(newArray)

        console.log(structuredDataforSS)
      });

      dataSB&& dataSB.forEach(object => {
        let newArray=[];
      for (const key in object) {
       if (key !== "_id") {

         newArray.push({name:key,data0:object[key][0],data1:object[key][1],data2:object[key][2],data3:object[key][3]})
       }
     }
       setStructuredDataforSB(newArray)

       console.log(structuredDataforSB)
     });


     dataSSS&& dataSSS.forEach(object => {
      let newArray=[];
    for (const key in object) {
     if (key !== "_id") {

       newArray.push({name:key,data0:object[key][0],data1:object[key][1],data2:object[key][2],data3:object[key][3]})
     }
   }
     setStructuredDataforSSS(newArray)

     console.log(structuredDataforSSS)
   });


   dataSSB&& dataSSB.forEach(object => {
        let newArray=[];
        for (const key in object) {
        if (key !== "_id") {

          newArray.push({name:key,data0:object[key][0],data1:object[key][1],data2:object[key][2],data3:object[key][3]})
        }
      }
        setStructuredDataforSSB(newArray)

        console.log(structuredDataforSSB)
      });


      dataSP&& dataSP.forEach(object => {
        let newArray=[];
        for (const key in object) {
        if (key !== "_id") {

          newArray.push({name:key,data0:object[key][0],data1:object[key][1],data2:object[key][2],data3:object[key][3]})
        }
      }
        setStructuredDataforSP(newArray)

        console.log(structuredDataforSP)
      });


      dataSVP&& dataSVP.forEach(object => {
        let newArray=[];
        for (const key in object) {
        if (key !== "_id") {

          newArray.push({name:key,data0:object[key][0],data1:object[key][1],data2:object[key][2],data3:object[key][3]})
        }
      }
        setStructuredDataforSVP(newArray)

        console.log(structuredDataforSVP)
      });


    }
  },[dataSS, dataSB, dataSSS, dataSP, dataSVP])

  return (
    <div onClick={() => setTickerOptions(null)}>
      <Helmet>
        <title>TradeApe - Trade better with AI Sentiment Analysis</title>
        <link rel="canonical" href="https://www.tradeape.co" />
        <meta
          name="keywords"
          content="Tradeape, trading, trade, stock, stock market, bull, bear, candlestick, machine learning, data science, algotrading, nlp"
        />
        <meta
          name="description"
          content="TradeApe uses data science and artificial intelligence to understand market sentiments and forecast trends so that you can make better trading decisions. "
        />
        <meta
          property="og:title"
          content="TradeApe - Trade better with AI Sentiment Analysis"
        />
        <meta property="og:url" content="https://www.tradeape.co" />
        <meta property="og:type" content="website" />
        <meta property="og:site_name" content="TradeApe" />
        <meta
          property="og:description"
          content="TradeApe - Trade better with AI Sentiment Analysis"
        />
        <meta
          property="og:image"
          content="https://drive.google.com/file/d/1tC1VlsvmgkOfahexN6lc4xw_dPpOOVZ6/view?usp=sharing "
        />

        <meta name="robots" content="index,follow" />
      </Helmet>
      <div className="main-wrapper" style={{width: '100%', alignItems: 'center'}}>
        <div className="page-wrapper">
          <nav className="navbar">
            <MobileNavbar />
          </nav>
          <br />
          <br />
          <nav class="page-breadcrumb" style={{marginTop: '5%', marginLeft: '2%'}}>
					<ol class="breadcrumb">
						{/* <li class="breadcrumb-item">Prediction</li> */}
						<li class="breadcrumb-item active" aria-current="page" style={{color: 'azure'}}>Cryptocurrency</li>
					</ol>
				</nav>

        <div class="row" style={{marginTop: '1%', justifyItems: 'center', marginLeft: '0.1%', width: '99%'}}>

        <div class="col-md-6 grid-margin stretch-card">
              <div class="card" style={{backgroundColor: 'rgba(52, 52, 52, 0.1)'}}>
                <div class="card-body">
                  <h6 class="card-title">Strong Buy</h6>
                  {/* <p class="card-description">Add class <code>.table</code></p> */}
                  <div class="table-responsive">
                      <table class="table">
                        <thead>
                          <tr style={{textAlign: 'center'}}>
                            <th style={{fontSize: 11}}>Ticker</th>
                            <th style={{fontSize: 11}}>Date</th>
                            <th style={{fontSize: 11}}><i style={{width: '15px', height: '12px', marginLeft: '1px'}} data-feather="triangle"></i> Mentions</th>
                            <th style={{fontSize: 11}}><i style={{width: '15px', height: '12px', marginLeft: '1px'}} data-feather="triangle"></i> Sentiment Score</th>
                            <th style={{fontSize: 11}}>Status</th>
                          </tr>
                        </thead>
                        <tbody>

                        {structuredDataforSSB&& structuredDataforSSB.map((item) => 
                            <tr style={{textAlign: 'center'}}>
                              <td style={{fontSize: 13}}>{item.name}</td>
                              <td style={{fontSize: 12}}>{moment( item.data3).format("DD-MM-YY")}</td>
                              <td style={{fontSize: 13}}>{item.data1}</td>
                              <td style={{fontSize: 13}}>{parseInt(item.data0)}</td>
                              <td style={{fontSize: 13}}>{item.data2}</td>
                            </tr>
                        )}
                        </tbody>
                      </table>
                      {/* {() => {
                        if(structuredDataforSSB) {
                          return(
                            <nav aria-label="Page navigation example">
                            <ul class="pagination">
                              <li class="page-item"><a class="page-link" href="#"><i data-feather="chevron-left"></i></a></li>
                              <li class="page-item active"><a class="page-link" href="#">1</a></li>
                              <li class="page-item disabled"><a class="page-link" href="#">2</a></li>
                              <li class="page-item"><a class="page-link" href="#">3</a></li>
                              <li class="page-item"><a class="page-link" href="#"><i data-feather="chevron-right"></i></a></li>
                            </ul>
                          </nav>
                          );
                        }
                      }} */}
                  </div>
                </div>
              </div>
            </div>


          <div class="col-md-6 grid-margin stretch-card">
            <div class="card" style={{backgroundColor: 'rgba(52, 52, 52, 0.1)'}}>
              <div class="card-body">
								<h6 class="card-title">Buy</h6>
								{/* <p class="card-description">Add class <code>.table</code></p> */}
								<div class="table-responsive">
										<table class="table">
											<thead>
												<tr style={{textAlign: 'center'}}>
                          <th style={{fontSize: 11}}>Ticker</th>
                          <th style={{fontSize: 11}}>Date</th>
													<th style={{fontSize: 11}}>No of Mention</th>
                          <th style={{fontSize: 11}}>Sentiment Score</th>
                          <th style={{fontSize: 11}}>Status</th>
												</tr>
											</thead>
											<tbody>

                      {structuredDataforSB&& structuredDataforSB.map((item) => 
                          <tr style={{textAlign: 'center'}}>
                            <td style={{fontSize: 13}}>{item.name}</td>
                            <td style={{fontSize: 12}}>{moment( item.data3).format("DD-MM-YY")}</td>
                            <td style={{fontSize: 13}}>{item.data1}</td>
                            <td style={{fontSize: 13}}>{parseInt(item.data0)}</td>
                            <td style={{fontSize: 13}}>{item.data2}</td>
                          </tr>
                      )}
											</tbody>
										</table>
								</div>
              </div>
            </div>
					</div>

				</div>


        <div class="row" style={{marginTop: '1%', justifyItems: 'center', marginLeft: '0.1%', width: '99%'}}>

            <div class="col-md-6 grid-margin stretch-card">
              <div class="card" style={{backgroundColor: 'rgba(52, 52, 52, 0.1)'}}>
                <div class="card-body">
                  <h6 class="card-title">Strong Sell</h6>
                  {/* <p class="card-description">Add class <code>.table</code></p> */}
                  <div class="table-responsive">
                      <table class="table">
                        <thead>
                          <tr style={{textAlign: 'center'}}>
                            <th style={{fontSize: 11}}>Ticker</th>
                            <th style={{fontSize: 11}}>Date</th>
                            <th style={{fontSize: 11}}>No of Mention</th>
                            <th style={{fontSize: 11}}>Sentiment Score</th>
                            <th style={{fontSize: 11}}>Status</th>
                          </tr>
                        </thead>
                        <tbody>

                        {structuredDataforSSS&& structuredDataforSSS.map((item) => 
                            <tr style={{textAlign: 'center'}}>
                            <td style={{fontSize: 13}}>{item.name}</td>
                            <td style={{fontSize: 12}}>{moment( item.data3).format("DD-MM-YY")}</td>
                            <td style={{fontSize: 13}}>{item.data1}</td>
                            <td style={{fontSize: 13}}>{parseInt(item.data0)}</td>
                            <td style={{fontSize: 13}}>{item.data2}</td>
                            </tr>
                        )}
                        </tbody>
                      </table>
                  </div>
                </div>
              </div>
            </div>


            <div class="col-md-6 grid-margin stretch-card">
            <div class="card" style={{backgroundColor: 'rgba(52, 52, 52, 0.1)'}}>
              <div class="card-body">
								<h6 class="card-title">Sell</h6>
								{/* <p class="card-description">Add class <code>.table</code></p> */}
								<div class="table-responsive">
										<table class="table">
											<thead>
												<tr style={{textAlign: 'center'}}>
													<th style={{fontSize: 11}}>Ticker</th>
                          <th style={{fontSize: 11}}>Date</th>
													<th style={{fontSize: 11}}>No of Mention</th>
                          <th style={{fontSize: 11}}>Sentiment Score</th>
                          <th style={{fontSize: 11}}>Status</th>
												</tr>
											</thead>
											<tbody>

                      {structuredDataforSS&& structuredDataforSS.map((item) => 
                          <tr style={{textAlign: 'center'}}>
                            <td style={{fontSize: 13}}>{item.name}</td>
                            <td style={{fontSize: 12}}>{moment( item.data3).format("DD-MM-YY")}</td>
                            <td style={{fontSize: 13}}>{item.data1}</td>
                            <td style={{fontSize: 13}}>{parseInt(item.data0)}</td>
                            <td style={{fontSize: 13}}>{item.data2}</td>
                          </tr>
                      )}
											</tbody>
										</table>
								</div>
              </div>
            </div>
					</div>

          </div>

          <div class="row" style={{marginTop: '1%', justifyItems: 'center', marginLeft: '0.1%', width: '99%'}}>

          <div class="col-md-6 grid-margin stretch-card">
                <div class="card" style={{backgroundColor: 'rgba(52, 52, 52, 0.1)'}}>
                  <div class="card-body">
                    <h6 class="card-title">Very Popular</h6>
                    {/* <p class="card-description">Add class <code>.table</code></p> */}
                    <div class="table-responsive">
                        <table class="table">
                          <thead>
                            <tr style={{textAlign: 'center'}}>
                              <th style={{fontSize: 11}}>Ticker</th>
                              <th style={{fontSize: 11}}>No of Mention</th>
                              <th style={{fontSize: 11}}>Status</th>
                              <th style={{fontSize: 11}}>Date</th>
                            </tr>
                          </thead>
                          <tbody>

                          {structuredDataforSVP&& structuredDataforSVP.map((item) => 
                              <tr style={{textAlign: 'center'}}>
                                <td>{item.name}</td>
                                <td>{parseInt(item.data0)}</td>
                                <td>{item.data1}</td>
                                <td>{moment( item.data2).format("DD-MM-YY")}</td>
                              </tr>
                          )}
                          </tbody>
                        </table>
                    </div>
                  </div>
                </div>
              </div>
              
              <div class="col-md-6 grid-margin stretch-card">
                <div class="card" style={{backgroundColor: 'rgba(52, 52, 52, 0.1)'}}>
                  <div class="card-body">
                    <h6 class="card-title">Popular</h6>
                    {/* <p class="card-description">Add class <code>.table</code></p> */}
                    <div class="table-responsive">
                        <table class="table">
                          <thead>
                            <tr style={{textAlign: 'center'}}>
                              <th style={{fontSize: 11}}>Ticker</th>
                              <th style={{fontSize: 11}}>No of Mention</th>
                              <th style={{fontSize: 11}}>Status</th>
                              <th style={{fontSize: 11}}>Date</th>
                            </tr>
                          </thead>
                          <tbody>

                          {structuredDataforSP&& structuredDataforSP.map((item) => 
                              <tr style={{textAlign: 'center'}}>
                                <td>{item.name}</td>
                                <td>{parseInt(item.data0)}</td>
                                <td>{item.data1}</td>
                                <td>{moment( item.data2).format("DD-MM-YY")}</td>
                              </tr>
                          )}
                          </tbody>
                        </table>
                        {/* <nav aria-label="Page navigation example">
                            <ul class="pagination">
                              <li class="page-item"><a class="page-link" href="#"><i data-feather="chevron-left"></i></a></li>
                              <li class="page-item active"><a class="page-link" href="#">1</a></li>
                              <li class="page-item disabled"><a class="page-link" href="#">2</a></li>
                              <li class="page-item"><a class="page-link" href="#">3</a></li>
                              <li class="page-item"><a class="page-link" href="#"><i data-feather="chevron-right"></i></a></li>
                            </ul>
                        </nav> */}
                    </div>
                  </div>
                </div>
              </div>

            </div>

          </div>
          </div>
		  </div>
  );
};

export default Prediction;
