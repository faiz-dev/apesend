import React, { useState, useEffect, useContext } from "react";
import Chart from "react-apexcharts";
import moment from "moment";

import UnitContext from "../context/context";
import { fetchGraphCryptoData } from "../utils/functions";

const CryptoGraph = ({ width, ticker }) => {
  const context = useContext(UnitContext);
  const [state, setState] = useState({
    options: {
      chart: {
        id: "bar",
        type: "line",
        background:
          "linear-gradient(180deg, rgba(0, 255, 76,0.3) 10%, rgba(255, 0, 0,0.3) 100%)",
      },
      colors: [
        "#94d0cc",
        "#907fa4",
        "#de8971",
        "#f21170",
        "#727cf5",
        "#ff6701",
      ],
      xaxis: {
        show: true,
        categories: [],
        labels: {
          style: {
            colors: "#f5f2f3",
          },
        },
        style: {
          color: "#f1ecee",
        },
      },
      yaxis: [
        {
          axisTicks: {
            show: true,
          },
          axisBorder: {
            show: true,
            color: "#f8f7f8",
          },
          labels: {
            style: {
              colors: "#f5f2f3",
            },
          },
          title: {
            style: {
              color: "#f1ecee",
            },
          },
        },
      ],

      grid: {
        borderColor: "rgba(77, 138, 240, .1)",
      },
      stroke: {
        curve: "smooth",
      },
    },
    series: [],
  });

  useEffect(() => {
    setState((prevState) => ({
      options: {
        ...prevState.options,
        xaxis: {
          ...prevState.options.xaxis,
          categories: [],
        },
      },
      series: [],
    }));
    (async () => {
      const series = [];
      const categories = [];
      let apiData = null;
      try {
        const res = await fetchGraphCryptoData(ticker, context.state.unit);
        apiData = res.data;
      } catch (error) {
        console.log("Graph error = ", error);
      }

      for (const key in apiData) {
        const values = [];
        apiData[key].forEach(({ point, value }, i) => {
          values.push(Number(value * 100).toFixed(2));
          categories.push(point);
        });
        series.push({
          name: key,
          data:
            context.state.unit === "daily"
              ? values.reverse().slice(0, 14)
              : values.reverse(),
          dataLabels: {
            enabled: false,
          },
        });
      }
      setState((prevState) => ({
        options: {
          ...prevState.options,
          xaxis: {
            ...prevState.options.xaxis,
            categories:
              context.state.unit === "daily"
                ? categories.reverse().slice(0, 14)
                : categories.reverse(),
          },
        },
        series: series,
      }));
    })();
  }, [context.state.unit, ticker]);

  return (
    <div className="app">
      <div className="row">
        <div className="mixed-chart" style={{ overflowX: "auto" }}>
          {state.options.xaxis.categories.length > 0 &&
            state.series.length > 0 && (
              <Chart
                options={state.options}
                series={state.series}
                width={width}
                height="400"
              />
            )}
        </div>
      </div>
    </div>
  );
};

export default CryptoGraph;
