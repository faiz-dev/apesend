import React from "react";
import { Link } from "react-router-dom";

class NavBar extends React.Component {
  render() {
    return (
      <nav className="sidebar">
        <div className="sidebar-header">
          <a href="#" className="sidebar-brand">
            <img src="stockape_logo.png" width="130px" />
          </a>
          <div className="sidebar-toggler not-active">
            <span></span>
            <span></span>
            <span></span>
          </div>
        </div>
        <div className="sidebar-body">
          <ul className="nav">
            <li className="nav-item nav-category" style={{ textAlign: "left" }}>
              STOCK
            </li>
            <Link to="/">
              <li className="nav-item">
                <a href="" className="nav-link">
                  {/* <i className="link-icon" data-feather="box"></i> */}
                  <svg
                    style={{ color: "white" }}
                    xmlns="http://www.w3.org/2000/svg"
                    width="16"
                    height="16"
                    viewBox="0 0 24 24"
                    fill="none"
                    stroke="currentColor"
                    stroke-width="3"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    className="feather feather-box link-icon"
                  >
                    <path d="M21 16V8a2 2 0 0 0-1-1.73l-7-4a2 2 0 0 0-2 0l-7 4A2 2 0 0 0 3 8v8a2 2 0 0 0 1 1.73l7 4a2 2 0 0 0 2 0l7-4A2 2 0 0 0 21 16z" />
                    <polyline points="3.27 6.96 12 12.01 20.73 6.96" />
                    <line x1="12" y1="22.08" x2="12" y2="12" />
                  </svg>
                  <span className="link-title">Overview</span>
                </a>
              </li>
            </Link>
            <Link to="/analysis">
              <li className="nav-item">
                <a href="" className="nav-link">
                  {/* <i className="link-icon" data-feather="pie-chart"></i> */}
                  <svg
                    style={{ color: "white" }}
                    className="link-icon"
                    xmlns="http://www.w3.org/2000/svg"
                    width="16"
                    height="16"
                    viewBox="0 0 24 24"
                    fill="none"
                    stroke="currentColor"
                    stroke-width="3"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    className="feather feather-pie-chart link-icon"
                  >
                    <path d="M21.21 15.89A10 10 0 1 1 8 2.83" />
                    <path d="M22 12A10 10 0 0 0 12 2v10z" />
                  </svg>
                  <span className="link-title">Search</span>
                </a>
              </li>
            </Link>
            <li className="nav-item nav-category" style={{ textAlign: "left" }}>
              CRYPTO
            </li>

            <Link to="/crypto_home">
              <li className="nav-item">
                <a href="" className="nav-link">
                  {/* <i className="link-icon" data-feather="pie-chart"></i> */}
                  <svg
                    style={{ color: "white" }}
                    className="link-icon"
                    xmlns="http://www.w3.org/2000/svg"
                    width="16"
                    height="16"
                    viewBox="0 0 24 24"
                    fill="none"
                    stroke="currentColor"
                    stroke-width="3"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    className="feather feather-pie-chart link-icon"
                  >
                    <path d="M21.21 15.89A10 10 0 1 1 8 2.83" />
                    <path d="M22 12A10 10 0 0 0 12 2v10z" />
                  </svg>
                  <span className="link-title">Overview</span>
                </a>
              </li>
            </Link>

            <Link to="/crypto_analysis">
              <li className="nav-item">
                <a href="" className="nav-link">
                  {/* <i className="link-icon" data-feather="pie-chart"></i> */}
                  <svg
                    style={{ color: "white" }}
                    className="link-icon"
                    xmlns="http://www.w3.org/2000/svg"
                    width="16"
                    height="16"
                    viewBox="0 0 24 24"
                    fill="none"
                    stroke="currentColor"
                    stroke-width="3"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    className="feather feather-pie-chart link-icon"
                  >
                    <path d="M21.21 15.89A10 10 0 1 1 8 2.83" />
                    <path d="M22 12A10 10 0 0 0 12 2v10z" />
                  </svg>
                  <span className="link-title">Search</span>
                </a>
              </li>
            </Link>

            <li className="nav-item nav-category" style={{ textAlign: "left" }}>
              Prediction
            </li>

            <Link to="/prediction">
              <li className="nav-item">
                <a href="" className="nav-link">
                  {/* <i className="link-icon" data-feather="pie-chart"></i> */}
                  <svg
                    style={{ color: "white" }}
                    className="link-icon"
                    xmlns="http://www.w3.org/2000/svg"
                    width="16"
                    height="16"
                    viewBox="0 0 24 24"
                    fill="none"
                    stroke="currentColor"
                    stroke-width="3"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    className="feather feather-pie-chart link-icon"
                  >
                    <path d="M21.21 15.89A10 10 0 1 1 8 2.83" />
                    <path d="M22 12A10 10 0 0 0 12 2v10z" />
                  </svg>
                  <span className="link-title"> Stocks</span>
                </a>
              </li>
            </Link>

            <Link to="/cryptoPrediction">
              <li className="nav-item">
                <a href="" className="nav-link">
                  {/* <i className="link-icon" data-feather="pie-chart"></i> */}
                  <svg
                    style={{ color: "white" }}
                    className="link-icon"
                    xmlns="http://www.w3.org/2000/svg"
                    width="16"
                    height="16"
                    viewBox="0 0 24 24"
                    fill="none"
                    stroke="currentColor"
                    stroke-width="3"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    className="feather feather-pie-chart link-icon"
                  >
                    <path d="M21.21 15.89A10 10 0 1 1 8 2.83" />
                    <path d="M22 12A10 10 0 0 0 12 2v10z" />
                  </svg>
                  <span className="link-title"> Cryptocurrency</span>
                </a>
              </li>
            </Link>

            <li className="nav-item nav-category" style={{ textAlign: "left" }}>
              About
            </li>
            <Link to="/about">
              <li className="nav-item">
                <a href="" className="nav-link">
                  {/* <i className="link-icon" data-feather="message-square"></i> */}
                  <svg
                    style={{ color: "white" }}
                    xmlns="http://www.w3.org/2000/svg"
                    width="16"
                    height="16"
                    viewBox="0 0 24 24"
                    fill="none"
                    stroke="currentColor"
                    stroke-width="3"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    className="feather feather-message-square link-icon"
                  >
                    <path d="M21 15a2 2 0 0 1-2 2H7l-4 4V5a2 2 0 0 1 2-2h14a2 2 0 0 1 2 2z" />
                  </svg>
                  <span className="link-title">About</span>
                </a>
              </li>
            </Link>

            <Link to="/faq">
              <li className="nav-item">
                <a href="" className="nav-link">
                  {/* <i className="link-icon" data-feather="layout"></i> */}
                  <svg
                    style={{ color: "white" }}
                    xmlns="http://www.w3.org/2000/svg"
                    width="16"
                    height="16"
                    viewBox="0 0 24 24"
                    fill="none"
                    stroke="currentColor"
                    stroke-width="3"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    className="feather feather-layout link-icon"
                  >
                    <rect x="3" y="3" width="18" height="18" rx="5" ry="5" />
                    <line x1="3" y1="9" x2="21" y2="9" />
                    <line x1="9" y1="21" x2="9" y2="9" />
                  </svg>
                  <span className="link-title">FAQ</span>
                </a>
              </li>
            </Link>
            <Link to="/termsOfUse">
              <li className="nav-item">
                <a href="" className="nav-link">
                  {/* <i className="link-icon" data-feather="layout"></i> */}
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="16"
                    height="16"
                    viewBox="0 0 24 24"
                    fill="none"
                    stroke="#fff"
                    stroke-width="2"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    className="feather feather-layout link-icon"
                  >
                    <rect
                      x="2"
                      y="2"
                      width="20"
                      height="8"
                      rx="2"
                      ry="2"
                    ></rect>
                    <rect
                      x="2"
                      y="14"
                      width="20"
                      height="8"
                      rx="2"
                      ry="2"
                    ></rect>
                    <line x1="6" y1="6" x2="6.01" y2="6"></line>
                    <line x1="6" y1="18" x2="6.01" y2="18"></line>
                  </svg>

                  <span className="link-title">Terms Of Use</span>
                </a>
              </li>
            </Link>
            <Link to="/privacy">
              <li className="nav-item">
                <a href="" className="nav-link">
                  {/* <i className="link-icon" data-feather="unlock"></i> */}
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="16"
                    height="16"
                    viewBox="0 0 24 24"
                    fill="none"
                    stroke="#fff"
                    stroke-width="2"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    className="feather feather-layout link-icon"
                  >
                    <rect
                      x="3"
                      y="11"
                      width="18"
                      height="11"
                      rx="2"
                      ry="2"
                    ></rect>
                    <path d="M7 11V7a5 5 0 0 1 10 0v4"></path>
                  </svg>
                  <div>
                    <span className="link-title">Privacy Policy</span>
                  </div>
                </a>
              </li>
            </Link>
          </ul>
        </div>
      </nav>
    );
  }
}

export default NavBar;
