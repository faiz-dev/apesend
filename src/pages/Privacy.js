import React from "react";
import MobileNavbar from "./../components/MobileNavbar/MobileNavbar";

const PrivacyPolicy = () => {
  return (
    <div className="main-wrapper">
      <div className="page-wrapper">
        <nav className="navbar">
          <MobileNavbar href="#" />
        </nav>
        <br />
        <br />
        <br />
        <div className="container pt-3">
          <div className="faq">
            <h1>Privacy Policy</h1>
            <br></br>
            <p>
              TradeApe cares about your privacy and is fully committed to
              protecting and safeguarding the personal data you share with us
              when you use our services. In this Privacy Policy, we explain what
              kind of data we use, and how we use it.
              <br></br>
              <br></br>
              We might amend this Privacy Policy from time to time. Please visit
              this page regularly in order to understand what we do and know our
              updates.
              <br></br>
              <br></br>
              We have designed our services with your privacy in mind. While
              TradeApe would be happy to hear feedback from you about how we can
              further strengthen this commitment to privacy, please note that if
              you do not agree with our processing of personal data as described
              in this Privacy Policy, you cannot continue the use of our
              Services.
            </p>
            <h2>What data do we collect?</h2>
            <br></br>
            <p>
              If you register an account, we collect the following minimal
              requirements for account sign up and login:
              <br></br>
              <br></br>
              Personal Information: Data required for account registration and
              login such as username, email, date of birth, gender and passwords
              <br></br>
              Financial Profile: Data may be solicited to construe a financial
              profile of you such as risk profile, investment objectives,
              financial standing etc<br></br>
              Payment Information: Data required for payment and subscriptions
              for premium users such as your bank, credit card and payment
              related details. Please note that financial information on
              TradeApe is processed by Stripe, as such, please check with
              Stripe’s detailed Privacy Policy.<br></br> TradeApe does not
              control and cannot be responsible for data obtained, processed or
              stored on Stripe.
            </p>
            <h2>Private data could be used for the following:</h2>
            <br></br>
            <p>
              Personalizing user experience: Data could be used to customize
              your user experience throughout the site<br></br>
              Marketing: Data could be used to offer or promote events and
              experiences, of both commercial and non-commercial nature to all
              users<br></br>
              Research: Data could be processed and pseudonymized to improve
              technology and user experience<br></br>
              To fulfill any other purposes for which you provide us private
              data and/or for any purposes for which you gave us authorization
              <br></br>
              By using our services, you explicitly consent to the data being
              processed for the purposes above or to advance the interests of
              TradeApe. As such, your data may be reviewed by our employees or
              third-party consultants who work for us and who are bound by
              strict confidentiality.
            </p>
            <h2>Social Media</h2>
            <br></br>
            <p>
              We may integrate social media like/share buttons which allow you
              to share references to TradeApe. These social media extensions may
              process your personal data when you choose to do so. TradeApe does
              not control and is not responsible for, the processing of personal
              data by these networks.
            </p>
            <h2>Data Policy</h2>
            <br></br>
            <p>
              Data provided to us are stored on secure cloud servers operated by
              trusted major companies. These servers may or may not be fixed to
              a certain specific location (unless expressly declared below), but
              are all held to similarly high standards. We will take the
              appropriate measures to prevent unauthorised access and/or misuse
              of your data. Consequently, your personal information may be
              transferred to, and stored at, a destination outside your country.
              It may also be handled by staff or by other third party service
              providers. We will take reasonable steps to ensure your data is
              handled securely. We also cannot be responsible for any actions
              caused by external providers who are beyond our control. By using
              our services, you acknowledge and consent to this.
              <br></br>
              <br></br>
              Data Retention<br></br>
              <br></br> TradeApe may periodically destroy or de-identify
              collected data once it is no longer required for the purpose or
              purposes for which it was collected.
            </p>

            <h2>Data of Children</h2>
            <br></br>

            <p>
              Investing is not for children. TradeApe does not support the use
              of our services for minors.
            </p>

            <h2>Your access to your data</h2>
            <br></br>

            <p>
              TradeApe is built to be end-to-end secured. This means that in the
              event you lose your email or password or any means to recover your
              account, you may lose access to your account and data permanently.
              We could not trust manual attempts to recover such information and
              we will not be obliged to do so. In such an event, please inform
              our support team that your account could be compromised and
              TradeApe would attempt to delete all relevant private data.
            </p>
            <h2>Your Responsibilities to your Data</h2>
            <br></br>

            <p>
              You have an obligation to protect your own data by ensuring proper
              security, protection and to safeguard your passwords, email
              address and recovery codes. Please do not reuse passwords or store
              it unsecurely. Please use a strong password combination and change
              it at regular intervals. Please do not share your passwords or
              your account.
            </p>
            <h2>And hence..</h2>
            <br></br>

            <p>
              TradeApe may revise and update content provided and/or Privacy
              Policy at any time. Your continued usage of the TradeApe will mean
              you accept those changes.
              <br></br>
              <br></br>
              If at any time you have concerns, please stop using our services
              immediately and contact us. If you are unable to accept these
              terms, please stop using our services.
              <br></br>
              <br></br>
              Last updated: 1 August 2021<br></br>
              <a
                href=""
                style={{ textDecoration: "underline" }}
                className="text-primary"
              >
                https://investorplace.com/corporate/terms-conditions/
              </a>
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default PrivacyPolicy;
