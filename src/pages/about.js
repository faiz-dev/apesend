import React from "react";

import twitter from "../images/icon-twitter-white.svg";
import reddit from "../images/icon-reddit-white.svg";
import linkedin from "../images/icon-linkedin-white.svg";
import gorillaImg from "../images/gorilla-about.png";
import { Menu } from "react-feather";
import MobileNavbar from "./../components/MobileNavbar/MobileNavbar";

const About = () => {
  return (
    <div className="main-wrapper">
      <div className="page-wrapper">
        <nav className="navbar ">
          <MobileNavbar />
        </nav>
        <br />
        <br />
        <br />
        <div className="container pt-3">
          <div className="about">
            <h2>Our Story</h2>
            <div className="mt-3 mb-3">
              <img src={gorillaImg} alt="" />
            </div>
            <h1> Apes Stronger with Artificial Intelligence</h1>
            <div className="pt-4">
              <p>
                TradeApe uses data science and artificial intelligence to
                understand market sentiments and forecast trends so that you can
                make better trading decisions.
              </p>
              <p>
                For too long, big data sentiment analytics are expensive and out
                of reach for the common man. TradeApe democratizes this industry
                to level the playing field.
              </p>
              <p>
                Founded in 2021 and headquartered in Singapore, TradeApe is
                keenly investing extensively in R&D to make trading more
                profitable for everyone.
              </p>
              <p>
                We hope to hear from you! Say hi at{" "}
                <a
                  href=""
                  style={{ textDecoration: "underline" }}
                  className="text-primary"
                >
                  hello@tradeape.co
                </a>{" "}
                for collaborations!
              </p>
            </div>
            <div>
              <img src={twitter} alt="" />
              <img src={reddit} alt="" />
              <img src={linkedin} alt="" />
            </div>
            <div className="disclaimer">
              <p>
                <span> Disclaimer:</span>
                Investing is risky and can result in financial ruin. Information
                provided is for general educational purposes and does not
                constitute financial advice or recommendation. We do not
                guarantee the accuracy or completeness of our information.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default About;
