import React, { useState, useEffect } from "react";
import { Helmet } from "react-helmet";
import { Link, useHistory } from "react-router-dom";

import CryptoGraph from "./CryptoGraph";
import CryptoDonut from "./Graph/CryptoDonut";
import CryptoAreaChart from "./Graph/CryptoArea";
import CryptoBar from "./Graph/CryptoBar";
import { searchCrypto } from "../utils";
import ProjectsCryptoTable from "../components/common/projectsTable/projectsCryptoTable";
import twitter from "../images/icon-twitter-white.svg";
import reddit from "../images/icon-reddit-white.svg";
import linkedin from "../images/icon-linkedin-white.svg";
import UnitSelector from "../components/common/unitSelector";
import { Menu } from "react-feather";
import MobileNavbar from "../components/MobileNavbar/MobileNavbar";

const CryptoHome = () => {
  const history = useHistory();
  const [tickerOptions, setTickerOptions] = useState(null);
  const [enteredTicker, setEnteredTicker] = useState("");

  const onSearchingTicker = async () => {
    try {
      const res = await searchCrypto(enteredTicker);
      const { data } = res;
      setTickerOptions(data.slice(0, 5));
    } catch (error) {
      console.log("error on searching");
    }
  };

  useEffect(() => {
    if (
      enteredTicker === "" ||
      (!/[a-z]/.test(enteredTicker) && /[A-Z]/.test(enteredTicker))
    ) {
      setTickerOptions(null);
    } else {
      onSearchingTicker();
    }
  }, [enteredTicker]);

  return (
    <div onClick={() => setTickerOptions(null)}>
      <Helmet>
        <title>TradeApe - Trade better with AI Sentiment Analysis</title>
        <link rel="canonical" href="https://www.tradeape.co" />
        <meta
          name="keywords"
          content="Tradeape, trading, trade, stock, stock market, bull, bear, candlestick, machine learning, data science, algotrading, nlp"
        />
        <meta
          name="description"
          content="TradeApe uses data science and artificial intelligence to understand market sentiments and forecast trends so that you can make better trading decisions. "
        />
        <meta
          property="og:title"
          content="TradeApe - Trade better with AI Sentiment Analysis"
        />
        <meta property="og:url" content="https://www.tradeape.co" />
        <meta property="og:type" content="website" />
        <meta property="og:site_name" content="TradeApe" />
        <meta
          property="og:description"
          content="TradeApe - Trade better with AI Sentiment Analysis"
        />
        <meta
          property="og:image"
          content="https://drive.google.com/file/d/1tC1VlsvmgkOfahexN6lc4xw_dPpOOVZ6/view?usp=sharing "
        />

        <meta name="robots" content="index,follow" />
      </Helmet>
      <div className="main-wrapper">
        <div className="page-wrapper">
          <nav className="navbar">
            <MobileNavbar href="#" />
            <div className="navbar-content">
              <form className="search-form" autoComplete="off">
                <div className="input-group">
                  <div className="input-group-prepend">
                    <div className="input-group-text">
                      <i data-feather="search"></i>
                    </div>
                  </div>
                  <input
                    autoComplete="off"
                    value={enteredTicker}
                    type="text"
                    className="form-control"
                    id="navbarForm"
                    placeholder="Search here..."
                    onChange={(e) => setEnteredTicker(e.target.value)}
                  />
                  <div
                    id="myInputautocomplete-list("
                    className="autocomplete-items"
                  >
                    {tickerOptions?.map((ticker) => (
                      <div
                        onClick={(e) => {
                          setEnteredTicker(ticker.Symbol);
                          setTickerOptions(null);
                          history.push(`/tickers/${ticker.Symbol}`);
                        }}
                        key={ticker._id}
                      >
                        ${ticker.Symbol} ({ticker["Name 1"]})
                      </div>
                    ))}
                  </div>
                </div>
              </form>
              <ul className="navbar-nav">
                <li className="nav-item dropdown">
                  <Link to="/login">
                    <a href="" className="nav-link">
                      <span className="link-title">Login</span>
                    </a>
                  </Link>
                </li>
              </ul>
            </div>
          </nav>
          <div className="page-content">
            <div className="d-flex justify-content-between align-items-center flex-wrap grid-margin">
              <div className="mt-3">
                <h4 className="mt-0 mb-md-0">Welcome to Dashboard</h4>
              </div>
              <div className="mt-3">
                <UnitSelector />
              </div>
            </div>

            <div className="row">
              <div className="col-xl-3 grid-margin stretch-card">
                <ProjectsCryptoTable />
              </div>
              <div className="col-xl-9 ">
                <div className="card overflow-hidden" id="myheight">
                  <div className="card-body">
                    <div className="d-flex justify-content-between align-items-baseline mb-4 mb-md-3">
                      <h6 className="card-title mb-0">
                        Live Sentiment Analysis
                      </h6>
                      <div className="dropdown">
                        <button
                          className="btn p-0"
                          type="button"
                          id="dropdownMenuButton3"
                          data-toggle="dropdown"
                          aria-haspopup="true"
                          aria-expanded="false"
                        >
                          <i
                            className="icon-lg text-muted pb-3px"
                            data-feather="more-horizontal"
                          />
                        </button>
                        <div
                          className="dropdown-menu dropdown-menu-right"
                          aria-labelledby="dropdownMenuButton3"
                        >
                          <a
                            className="dropdown-item d-flex align-items-center"
                            href="#"
                          >
                            <i data-feather="eye" className="icon-sm mr-2" />{" "}
                            <span className="">View</span>
                          </a>
                          <a
                            className="dropdown-item d-flex align-items-center"
                            href="#"
                          >
                            <i data-feather="edit-2" className="icon-sm mr-2" />{" "}
                            <span className="">Edit</span>
                          </a>
                          <a
                            className="dropdown-item d-flex align-items-center"
                            href="#"
                          >
                            <i data-feather="trash" className="icon-sm mr-2" />{" "}
                            <span className="">Delete</span>
                          </a>
                          <a
                            className="dropdown-item d-flex align-items-center"
                            href="#"
                          >
                            <i
                              data-feather="printer"
                              className="icon-sm mr-2"
                            />{" "}
                            <span className="">Print</span>
                          </a>
                          <a
                            className="dropdown-item d-flex align-items-center"
                            href="#"
                          >
                            <i
                              data-feather="download"
                              className="icon-sm mr-2"
                            />{" "}
                            <span className="">Download</span>
                          </a>
                        </div>
                      </div>
                    </div>
                    <div className="row align-items-start mb-2">
                      <div className="col-md-7">
                        <p
                          className="text-muted tx-13 mb-3 mb-md-0"
                          style={{ textAlign: "left" }}
                        >
                          TradeApe uses deep learning analysis on multiple live
                          data points to generate a trending aggregate sentiment
                          score. Graph will auto update every interval.
                        </p>
                      </div>
                    </div>

                    <div className="row">
                      <h6
                        id="newfont"
                        style={{ color: "green", margin: "10px 0" }}
                      >
                        Bullish
                      </h6>
                    </div>

                    <div className="flot-wrapper" style={{ height: "400px" }}>
                      <CryptoGraph className="flot-chart mygraph" width="860px" />
                    </div>
                    <div className="row">
                      <h6 id="newfont" style={{ color: "red", marginLeft: "" }}>
                        Bearish
                      </h6>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <br />
            <div className="row">
              <div className="col-xl-6 grid-margin stretch-card">
                <div className="card">
                  <div className="card-body">
                    <h6 className="card-title">MOST ACTIVE</h6>
                    <CryptoDonut />
                  </div>
                </div>
              </div>
              <div className="col-xl-6 grid-margin stretch-card">
                <div className="card">
                  <div className="card-body">
                    <h6 className="card-title">AWARDS MENTION</h6>
                    <CryptoBar type="ticker-awards" />
                  </div>
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-xl-6 grid-margin stretch-card">
                <div className="card">
                  <div className="card-body">
                    <h6 className="card-title">NUMBER OF MENTIONS</h6>
                    <CryptoBar type="ticker-occurInDoc" />
                  </div>
                </div>
              </div>
              <div className="col-xl-6 grid-margin stretch-card">
                <div className="card">
                  <div className="card-body">
                    <h6 className="card-title">UPVOTED AND DOWNVOTES</h6>
                    <CryptoAreaChart width={500} />
                  </div>
                </div>
              </div>
            </div>
          </div>

          <footer className="footer d-flex flex-column flex-md-row align-items-center justify-content-between">
            <p className="text-muted text-center text-md-left">
              Use is subject to full acceptance of{" "}
              <Link to="/termsOfUse">
                {" "}
                <a href="facebook.com">Terms of Use</a>{" "}
              </Link>
              and{" "}
              <Link to="/privacy">
                {" "}
                <a href="">Privacy Policy</a>
              </Link>
              , bar none. Investing is risky and can result in huge losses.
              <br /> Information provided is not financial advice and we do not
              warrant or guarantee the contents are accurate, up-to-date or
              reliable.{" "}
            </p>
            <p className="text-muted text-center text-md-left mb-0 d-md-block mt-2">
              TradeApe ©{" "}
              <a href="https://twitter.com/stock_ape">
                <img
                  style={{ width: "20px", height: "20px", margin: "0 2px" }}
                  src={twitter}
                  alt=""
                />
              </a>{" "}
              <a href="https://www.reddit.com/r/TradeApeAI/">
                <img
                  style={{ width: "20px", height: "20px", margin: "0 2px" }}
                  src={reddit}
                  alt=""
                />
              </a>
              <a href="https://www.linkedin.com/company/tradeape">
                <img
                  style={{ width: "20px", height: "20px", margin: "0 2px" }}
                  src={linkedin}
                  alt=""
                />
              </a>
            </p>
          </footer>
        </div>
      </div>
    </div>
  );
};

export default CryptoHome;
