import React from "react";
import { Menu } from "react-feather";
import MobileNavbar from "./../components/MobileNavbar/MobileNavbar";

const Faq = () => {
  return (
    <div className="main-wrapper">
      <div className="page-wrapper">
        <nav className="navbar">
          <MobileNavbar href="#" />
        </nav>
        <br />
        <br />
        <br />
        <div className="container pt-3">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-body">
                  <h6 class="card-title">Frequently Asked Questions</h6>
                  <div id="accordion" class="accordion" role="tablist">
                    <div class="card">
                      <div class="card-header" role="tab" id="headingOne">
                        <h6 class="mb-0">
                          <a
                            data-toggle="collapse"
                            href="#collapseOne"
                            aria-expanded="true"
                            aria-controls="collapseOne"
                          >
                            How can TradeApe.co help me?
                          </a>
                        </h6>
                      </div>
                      <div
                        id="collapseOne"
                        class="collapse show"
                        role="tabpanel"
                        aria-labelledby="headingOne"
                        data-parent="#accordion"
                      >
                        <div class="card-body">
                          TradeApe uses data science and machine learning
                          (natural language processing) to provide a wide array
                          of metrics for you to measure the current median
                          sentiment of any particular financial instrument so
                          that you can make better trading decisions.
                        </div>
                      </div>
                    </div>
                    <div class="card">
                      <div class="card-header" role="tab" id="headingTwo">
                        <h6 class="mb-0">
                          <a
                            class="collapsed"
                            data-toggle="collapse"
                            href="#collapseTwo"
                            aria-expanded="false"
                            aria-controls="collapseTwo"
                          >
                            How do you arrive at the data?{" "}
                          </a>
                        </h6>
                      </div>
                      <div
                        id="collapseTwo"
                        class="collapse"
                        role="tabpanel"
                        aria-labelledby="headingTwo"
                        data-parent="#accordion"
                      >
                        <div class="card-body">
                          TradeApe collects a wide and varying spectrum of data
                          points, both discrete and implied from a variety of
                          sources. Data is then processed, cleaned and
                          normalized. Machine learning via various architectures
                          and mathematical models, including but not limited to
                          neural networks, produce data, analysis, publications,
                          signals and reports (collectively “information”) which
                          we provide on our website.
                        </div>
                      </div>
                    </div>
                    <div class="card">
                      <div class="card-header" role="tab" id="headingThree">
                        <h6 class="mb-0">
                          <a
                            class="collapsed"
                            data-toggle="collapse"
                            href="#collapseThree"
                            aria-expanded="false"
                            aria-controls="collapseThree"
                          >
                            How do I invest with such data?
                          </a>
                        </h6>
                      </div>
                      <div
                        id="collapseThree"
                        class="collapse"
                        role="tabpanel"
                        aria-labelledby="headingThree"
                        data-parent="#accordion"
                      >
                        <div class="card-body">
                          We believe that sentiments can guide swing trades,
                          momentum trades and short term frequency trades. As
                          our project is still experimental in nature, please
                          only paper trade. In addition, always and only invest
                          money you can afford to lose and never leverage!
                        </div>
                      </div>
                    </div>
                    <div class="card">
                      <div class="card-header" role="tab" id="headingFour">
                        <h6 class="mb-0">
                          <a
                            class="collapsed"
                            data-toggle="collapse"
                            href="#collapseFour"
                            aria-expanded="false"
                            aria-controls="collapseFour"
                          >
                            What else should I know?
                          </a>
                        </h6>
                      </div>
                      <div
                        id="collapseFour"
                        class="collapse"
                        role="tabpanel"
                        aria-labelledby="headingFour"
                        data-parent="#accordion"
                      >
                        <div class="card-body">
                          Investing is risky and can result in financial ruin.
                          Information TradeApe provides is for general
                          educational purposes and does not constitute financial
                          advice or recommendation. We do not guarantee the
                          accuracy or completeness of our information. We cannot
                          be liable for your use of our data or content. Please
                          check the Terms of Use and Privacy Policy for more
                          information.
                        </div>
                      </div>
                    </div>
                    <div class="card">
                      <div class="card-header" role="tab" id="headingFive">
                        <h6 class="mb-0">
                          <a
                            class="collapsed"
                            data-toggle="collapse"
                            href="#collapseFive"
                            aria-expanded="false"
                            aria-controls="collapseFive"
                          >
                            What is TradeApe’s mission?
                          </a>
                        </h6>
                      </div>
                      <div
                        id="collapseFive"
                        class="collapse"
                        role="tabpanel"
                        aria-labelledby="headingFive"
                        data-parent="#accordion"
                      >
                        <div class="card-body">
                          TradeApe believes that everyone, even retail
                          investors, can trade better and make wiser decisions
                          with data science and Artificial Intelligence. These
                          advanced financial tools were previously only
                          available to big funds. Let’s return power to the
                          people!.
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Faq;
