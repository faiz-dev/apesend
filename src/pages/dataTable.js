/* eslint-disable no-unused-vars */
import React, { useState, useEffect, useCallback } from "react";
import { Helmet } from "react-helmet";
import { Link, useHistory } from "react-router-dom";
import axios from "axios";
import moment from 'moment';

import Graph from "./Graph";
import Donut from "./Graph/Donut";
import Area from "./Graph/Area";
import Bar from "./Graph/Bar";
import { searchTicker } from "../utils";
import ProjectsTable from "../components/common/projectsTable/projectsTable";
import twitter from "../images/icon-twitter-white.svg";
import reddit from "../images/icon-reddit-white.svg";
import linkedin from "../images/icon-linkedin-white.svg";
import UnitSelector from "../components/common/unitSelector";
import { Menu } from "react-feather";
import MobileNavbar from "../components/MobileNavbar/MobileNavbar";

function Prediction () {
  const history = useHistory();
  const [tickerOptions, setTickerOptions] = useState(null);
  const [enteredTicker, setEnteredTicker] = useState("");
  
  const [data, dataSet] = useState();
  const [structuredData,setStructuredData]=useState();

  
  const onSearchingTicker = async () => {

    try {
      const res = await searchTicker(enteredTicker);
      const { data } = res;
      setTickerOptions(data.slice(0, 5));
    } catch (error) {
      console.log("error on searching");
    }
  };

const fetchMyAPI = useCallback(async () => {
    let response = await axios('https://tradeape.co:8080/ss')
    // let response1 = await axios('https://tradeape.co:8080/cs')

    dataSet(response.data);
  }, [])

  useEffect(() => {
    fetchMyAPI();

    if (
      enteredTicker === "" ||
      (!/[a-z]/.test(enteredTicker) && /[A-Z]/.test(enteredTicker))
    ) {
      setTickerOptions(null);
    } else {
      onSearchingTicker();
    }
  }, [enteredTicker]);

  useEffect(() => {
    if (data) {

      data.forEach(object => {
         let newArray=[];
       for (const key in object) {
        if (key !== "_id") {

          newArray.push({name:key,data0:object[key][0],data1:object[key][1],data2:object[key][2],data3:object[key][3]})
        }
      }
        setStructuredData(newArray)

        console.log(structuredData)
      });


    }
  },[data])

  return (
    <div onClick={() => setTickerOptions(null)}>
      <Helmet>
        <title>TradeApe - Trade better with AI Sentiment Analysis</title>
        <link rel="canonical" href="https://www.tradeape.co" />
        <meta
          name="keywords"
          content="Tradeape, trading, trade, stock, stock market, bull, bear, candlestick, machine learning, data science, algotrading, nlp"
        />
        <meta
          name="description"
          content="TradeApe uses data science and artificial intelligence to understand market sentiments and forecast trends so that you can make better trading decisions. "
        />
        <meta
          property="og:title"
          content="TradeApe - Trade better with AI Sentiment Analysis"
        />
        <meta property="og:url" content="https://www.tradeape.co" />
        <meta property="og:type" content="website" />
        <meta property="og:site_name" content="TradeApe" />
        <meta
          property="og:description"
          content="TradeApe - Trade better with AI Sentiment Analysis"
        />
        <meta
          property="og:image"
          content="https://drive.google.com/file/d/1tC1VlsvmgkOfahexN6lc4xw_dPpOOVZ6/view?usp=sharing "
        />

        <meta name="robots" content="index,follow" />
      </Helmet>
      <div className="main-wrapper">
        <div className="page-wrapper">
        <div class="row">

					<div class="col-md-6 grid-margin stretch-card">
            <div class="card">
              <div class="card-body">
								<h6 class="card-title">Basic Table (ss)</h6>
								{/* <p class="card-description">Add class <code>.table</code></p> */}
								<div class="table-responsive">
										<table class="table">
											<thead>
												<tr>
													<th>Ticker</th>
													<th>Sentiment Score</th>
													<th>No. of Mention</th>
                          <th>Status</th>
                          <th>Date</th>
												</tr>
											</thead>
											<tbody>

                      {structuredData&& structuredData.map((item) => 
                          <tr>
                            <td>{item.name}</td>
                            <td style={{alignItems: 'center'}}>{parseFloat(item.data0).toFixed(5)}</td>
                            <td>{item.data1}</td>
                            <td>{item.data2}</td>
                            <td>{moment( item.data3).format("DD-MM-YY")}</td>
                          </tr>
                      )}
											</tbody>
										</table>
								</div>
              </div>
            </div>
					</div>
				</div>
          </div>
          </div>
		  </div>
  );
};

export default Prediction;
