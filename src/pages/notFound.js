import React from "react";
import { Link } from "react-router-dom";

const NotFound = () => {
  return (
    <div className="main-wrapper">
      <div className="page-wrapper">
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            flexFlow: "column",
            height: "100%",
          }}
        >
          <h4>Ticker Not Found</h4>
          <Link to="/">
            <h4 style={{ color: "#7DEDFF" }}>
              Click here to return to TradeApe.co
            </h4>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default NotFound;
