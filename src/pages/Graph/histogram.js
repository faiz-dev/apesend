import React, { useState, useEffect, useContext } from "react";
import Chart from "react-apexcharts";
import moment from "moment";

import UnitContext from "../../context/context";
import { fetchGraphData } from "../../utils";

const Histogram = ({ ticker }) => {
  const value = useContext(UnitContext);
  const [state, setState] = useState({
    options: {
      chart: {
        type: "bar",
        // height: "320",
        parentHeightOffset: 0,
      },
      colors: ["rgb(0,130,127)"],
      grid: {
        borderColor: "rgba(77, 138, 240, .1)",
        padding: {
          bottom: -10,
        },
      },
      xaxis: {
        type: "range",
        categories: [-1, 0, 0.79, 1],
      },
    },
    series: [],
  });

  useEffect(() => {
    (async () => {
      try {
        const res = await fetchGraphData(ticker, value.state.unit);
        const { data } = res;

        const seriesData = [];
        Object.values(data[ticker]).forEach(({ date, value }) => {
          seriesData.push(value);
          // values.push(value.toFixed(2));
        });
        setState((prevState) => ({
          ...prevState,
          series: [
            {
              name: ticker,
              data: seriesData,
            },
          ],
        }));
      } catch (error) {
        console.log("Bar error = ", error);
      }
    })();
  }, [ticker, value.state.unit]);

  return (
    <Chart
      options={state.options}
      series={state.series}
      type="bar"
      width="100%"
      height="300"
    />
  );
};

export default Histogram;
