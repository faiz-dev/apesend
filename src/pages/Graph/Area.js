import React, { useState, useEffect, useContext } from "react";
import { BarChart, Bar, XAxis, YAxis, Tooltip, Legend } from "recharts";

import UnitContext from "../../context/context";
import { fetchAreaData } from "../../utils";

export default function AreaChart({ ticker, width }) {
  const { state } = useContext(UnitContext);
  const [chartData, setChartData] = useState(null);

  useEffect(() => {
    const temp = [];
    (async (unit) => {
      try {
        const res = await fetchAreaData(unit, ticker);
        const { data } = res;
        if (ticker) {
          setChartData(
            Object.values(data[ticker]).map(({ point, upVotes, downVotes }) => {
              return {
                name: point,
                uv: upVotes,
                dv: downVotes,
              };
            })
          );
        } else {
          for (const key in data) {
            const bar = {
              name: key,
              uv: data[key][0].upVotes,
              dv: data[key][0].downVotes,
            };
            temp.push(bar);
          }
          setChartData(temp);
        }
      } catch (error) {
        console.log("Area chart error = ", error);
      }
    })(state.unit);
  }, [state.unit, ticker]);

  return (
    <div style={{ overflowX: "auto" }}>
      {chartData?.length > 0 && (
        <BarChart
          width={width}
          height={300}
          data={
            state.unit === "daily"
              ? chartData.reverse().slice(0, 14)
              : chartData.reverse()
          }
        >
          <XAxis dataKey="name" />
          <YAxis />
          <Tooltip />
          <Legend />
          <Bar dataKey="dv" stackId="a" fill="rgb(220,20,60)" />
          <Bar dataKey="uv" stackId="a" fill="	rgb(50,205,50)" />
        </BarChart>
      )}
    </div>
  );
}
