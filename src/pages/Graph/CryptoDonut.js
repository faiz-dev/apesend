import React, { useEffect, useState, useContext } from "react";
import Chart from "react-apexcharts";

import UnitContext from "../../context/context";
import { fetchCryptoData } from "../../utils";

const CryptoDonut = () => {
  const context = useContext(UnitContext);
  const [state, setState] = useState({
    options: {
      dataLabels: {
        enabled: false,
      },
      stroke: {
        colors: ["rgba(0,0,0,0)"],
      },
      colors: ["#3b06fb", "#947ef7", "#7ee5e5", "#4d8af0", "#fb0637"],
      legend: {
        position: "top",
        horizontalAlign: "center",
      },
      labels: [],
    },
    series: [],
  });

  useEffect(() => {
    const fetcher = async () => {
      const values = [];
      const labels = [];
      try {
        const res = await fetchCryptoData(context.state.unit);
        for (const key in res.data) {
          labels.push(key);
          values.push(
            res.data[key][0][
              context.state.unit === "hourly" ? "occurInDoc" : "occurence"
            ]
          );
        }
        setState((prevState) => ({
          series: values,
          options: { ...prevState, labels: labels },
        }));
      } catch (error) {
        console.log("Donut error = ", error);
      }
    };
    fetcher();
  }, [context.state.unit]);

  return (
    <>
      <Chart
        options={state.options}
        series={state.series}
        type="donut"
        width="100%"
        height="300"
      />
    </>
  );
};

export default CryptoDonut;
