import React, { useEffect, useState, useContext } from "react";
import Chart from "react-apexcharts";
import randomColor from "randomcolor";

import UnitContext from "../../context/context";
import { fetchAreaData } from "../../utils";

const Bar = ({ type, ticker }) => {
  const context = useContext(UnitContext);
  const [state, setState] = useState({
    options: {
      chart: {
        type: "bar",
        parentHeightOffset: 0,
      },
      colors: [
        randomColor({
          format: "hex",
        }),
      ],
      grid: {
        borderColor: "rgba(77, 138, 240, .1)",
        padding: {
          bottom: -10,
        },
      },
      xaxis: {
        type: "range",
        categories: [],
      },
    },
    series: [],
  });

  useEffect(() => {
    (async () => {
      if (type === "dateMention") {
        try {
          const res = await fetchAreaData(context.state.unit, ticker);
          const data = res.data;
          const dates = [];
          const mentions = [];
          Object.values(data[ticker]).forEach(({ occurInDoc, point }) => {
            dates.push(point);
            mentions.push(occurInDoc);
          });
          setState((prevState) => ({
            options: {
              ...prevState.options,
              labels:
                context.state.unit === "daily"
                  ? dates.reverse().slice(0, 14)
                  : dates.reverse(),
            },
            series: [
              {
                data:
                  context.state.unit === "daily"
                    ? mentions.reverse().slice(0, 14)
                    : mentions.reverse(),
              },
            ],
          }));
        } catch (err) {
          console.log(err);
        }
      } else if (type === "dateAwards") {
        try {
          const res = await fetchAreaData(context.state.unit, ticker);
          const data = res.data;
          const dates = [];
          const awardsArray = [];
          Object.values(data[ticker]).forEach(({ awards, point }) => {
            dates.push(point);
            awardsArray.push(awards);
          });
          setState((prevState) => ({
            options: {
              ...prevState.options,
              labels:
                context.state.unit === "daily"
                  ? dates.reverse().slice(0, 14)
                  : dates.reverse(),
            },
            series: [
              {
                data:
                  context.state.unit === "daily"
                    ? awardsArray.reverse().slice(0, 14)
                    : awardsArray.reverse(),
              },
            ],
          }));
        } catch (err) {
          console.log(err);
        }
      } else {
        const arr1 = [];
        const arr2 = [];
        let data = null;
        try {
          const res = await fetchAreaData(context.state.unit);
          data = res.data;

          for (const key in data) {
            arr1.push(key);
            arr2.push(data[key][0][type.split("-")[1]]);
          }
          setState((prevState) => ({
            options: { ...prevState.options, labels: arr1 },
            series: [
              {
                data: arr2,
              },
            ],
          }));
        } catch (error) {
          console.log(error);
        }
      }
    })();
  }, [context.state.unit, type, ticker]);

  return (
    <>
      {state.options?.labels?.length > 0 && state.series.length > 0 && (
        <Chart
          options={state.options}
          series={state.series}
          type="bar"
          width="100%"
          height="300"
        />
      )}
    </>
  );
};

export default Bar;
