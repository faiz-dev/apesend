import React from "react";
import MobileNavbar from "./../components/MobileNavbar/MobileNavbar";

const Terms = () => {
  return (
    <div className="main-wrapper">
      <div className="page-wrapper">
        <nav className="navbar">
          <MobileNavbar href="#" />
        </nav>
        <br />
        <br />
        <br />
        <div className="container pt-3">
          <div className="faq">
            <h1>Terms of Use, Disclosures and Disclaimers</h1>
            <br></br>
            <p>
              TradeApe believes that everyone, even retail investors, can trade
              better and make wiser decisions with data science and Artificial
              Intelligence.<br></br>
              <br></br>
              Please carefully read, understand and accept these terms,
              disclaimers and policies in order to continue using our services.
              Your continued usage will mean that you have accepted all our
              policies.
            </p>
            <h2>Methodology</h2>
            <br></br>
            <p>
              TradeApe collects a wide and varying spectrum of data points, both
              discrete and implied from a variety of sources. Data is then
              processed, cleaned and normalized. Machine learning via various
              architectures and mathematical models, including but not limited
              to neural networks, produce data, analysis, publications, signals
              and reports (collectively “information”) which we provide on our
              website, either for free or for a fee.
              <br></br>
              <br></br>
              Information supplied by our services are generated automatically
              as per the processes above. However, our staff / admin team may
              also manually add, update or delete information.
            </p>
            <h2>Artificial Intelligence & Data Science</h2>
            <br></br>
            <p>
              TradeApe uses proprietary data science methods and artificial
              intelligence, which are highly experimental and non-peer reviewed.
              Moreover, information is usually generated live from this "black
              box" automatically and beyond human control.
            </p>
            <h2>Conflict of Interest and Disclosure</h2>
            <br></br>
            <p>
              TradeApe and the staff may hold positions or interests, long or
              short, in the securities and/or their derivatives shown on our
              platform.
            </p>
            <h2>Risk Warning</h2>
            <br></br>
            <p>
              Investing and trading are risky by nature and can result in
              financial losses. Trading derivatives and cryptocurrencies is even
              more dangerous and can lead to complete financial write-down due
              to potential leverages. Please be aware of your personal financial
              standing, risk profile and only invest what you can afford to
              lose. Never ever leverage.
            </p>

            <h2>No Recommendation or Advice</h2>
            <br></br>

            <p>
              Information supplied or found on our website or services, stated,
              perceived or implied, are general information for general
              educational purposes only.<br></br>
              <br></br>
              None of the information constitutes an offer (or solicitation of
              an offer), a financial recommendation and/or advice, to buy or
              sell any financial instrument, to make any investment, or to
              participate in any particular trading strategy.
            </p>

            <h2>No Guarantee or Promises</h2>
            <br></br>

            <p>
              TradeApe does not guarantee, imply or warrant that information and
              publications supplied are accurate, up-to-date, complete or
              applicable to any circumstances.
              <br></br>
              <br></br>
              Expressions of opinions are personal to the authors and we make no
              guarantee of any sort regarding accuracy or completeness of any
              information or analysis supplied.<br></br>
              <br></br>
              Any reliance on any information, publications or services by you
              is at your own discretion and risk. Caveat emptor applies.
            </p>
            <h2>Limitation of Liabilities</h2>
            <br></br>

            <p>
              The authors and TradeApe are not responsible for any loss arising
              from any investment based on any perceived recommendation,
              forecast or any other information contained here. The contents of
              these publications should not be construed as an express or
              implied promise, guarantee or implication by TradeApe that clients
              will profit or that losses in connection therewith can or will be
              limited, from reliance on any information set out here.
              <br></br>
              <br></br>
              As such, the authors and TradeApe do not assume any liability or
              responsibility for damages, losses or injury to you, other
              persons, or property arising from any use of any information
              provided by us. If you live in a jurisdiction which does not allow
              limitations on how long an implied warranty lasts and
              jurisdictions which do not allow the exclusion or limitation of
              damages, or the limitation of liability to specified amounts (at
              maximum $1 (ONE) Singapore dollar, to premium accounts only),
              please do not use our services or access TradeApe.
              <br></br>
              <br></br>
              Our services are intended for personal and retail use only.
              Therefore TradeApe will not be liable in respect of any business
              losses, including (without limitation) loss of or damage to
              profits, income, revenue, use, production, anticipated savings,
              business, contracts, commercial opportunities or goodwill. We will
              also not be liable to you in respect of any loss or corruption of
              any data, database or software.
            </p>
            <h2>Indemnification</h2>
            <br></br>

            <p>
              You agree to defend, indemnify, and hold harmless TradeApe from
              all liabilities, claims, and expenses, including attorneys' fees,
              that arise from your use of our services. TradeApe reserves the
              right, at its own expense, to assume the exclusive defense and
              control of any matter otherwise subject to indemnification by you,
              in which event you will cooperate with TradeApe in asserting any
              available defenses. If you live in a jurisdiction which does not
              allow such indemnifications, please do not use our services or
              access TradeApe.
            </p>
            <h2>Change of Policies or Information</h2>
            <br></br>

            <p>
              TradeApe does not imply, guarantee or warrant information and
              publications are accurate, up-to-date or applicable to the
              circumstances of any particular case. We may revise and update
              content, both retrospectively and prospectively provided and/or
              terms and policies at any time.
            </p>

            <h2>No Unlawful Use</h2>
            <br></br>

            <p>
              You agree not to use TradeApe for any purpose that is unlawful or
              prohibited by these terms, or the terms of your country. You may
              not use our services in any manner that could damage, disable,
              overburden, or impair our network, server and AI capabilities. You
              may not attempt to gain unauthorized access to any portion of
              TradeApe, computer systems, AI capabilities or content connected
              to any TradeApe server, through hacking, password or data mining,
              or any other means. You may not obtain or attempt to obtain any
              materials or information through any means not intentionally made
              available to you on TradeApe.
            </p>
            <h2>Discontinuance of Service and Modifications</h2>
            <br></br>

            <p>
              We reserve the right to modify, discontinue, temporarily or
              permanently, this site, or any part thereof, at any time, with or
              without notice. In addition, your access to the site may be
              discontinued at any time if you violate the Terms of this
              agreement or if we determine, for any other reason, that it is no
              longer appropriate for you to have access to the site.
            </p>
            <h2>And hence..</h2>
            <br></br>

            <p>
              Do not make any decision without first conducting your own
              research, due diligence and consulting with your personal
              financial advisor. Your continued usage of the TradeApe will mean
              you accept our terms and policies.
              <br></br>
              <br></br>
              If at any time you have concerns, please stop using our services
              immediately and contact us. If you are unable to accept these
              terms, please stop using our services.
              <br></br>
              <br></br>
              Last updated: 1 August 2021
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Terms;
