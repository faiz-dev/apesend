import axios from "axios";

const instance = axios.create({
  baseURL: "https://tradeape.co:8080/",
});

export default instance;
