import axios from "../axios";

export const fetchDonutData = () => {
  return axios.get("donut", {
    headers: {
      "Content-Type": "application/json",
    },
  });
};

export const fetchGraphData = (ticker, unit) => {
  return axios.get(
    `centiment?&step=1&unit=${unit}${ticker ? `&ticker=${ticker}` : ""}`,
    {
      headers: {
        "Content-Type": "application/json",
      },
    }
  );
};

export const fetchGraphCryptoData = (ticker, unit) => {
  return axios.get(
    `cryptcentiment?&step=1&unit=${unit}${ticker ? `&ticker=${ticker}` : ""}`,
    {
      headers: {
        "Content-Type": "application/json",
      },
    }
  );
};


export const fetchTableData = (unit) => {
  return axios.get(`table?unit=${unit}`, {
    headers: {
      "Content-Type": "application/json",
    },
  });
};

export const fetchBarData = (unit, ticker) => {
  return axios.get(`bar?unit=${unit}&ticker=${ticker}`, {
    headers: {
      "Content-Type": "application/json",
    },
  });
};

export const fetchAreaData = (unit, ticker) => {
  return axios.get(`votes?unit=${unit}${ticker ? `&ticker=${ticker}` : ""}`, {
    headers: {
      "Content-Type": "application/json",
    },
    // cancelToken: cancelTokenSource,
  });
};

export const fetchCryptoData = (unit, ticker) => {
  return axios.get(`cryptovotes?unit=${unit}${ticker ? `&ticker=${ticker}` : ""}`, {
    headers: {
      "Content-Type": "application/json",
    },
    // cancelToken: cancelTokenSource,
  });
};

export const addTicker = (data) => {
  return axios.post("add_ticker", data, {
    headers: {
      "Content-Type": "application/json",
    },
  });
};

export const addCrypto = (data) => {
  return axios.post("add_crypto", data, {
    headers: {
      "Content-Type": "application/json",
    },
  });
};

export const deleteTicker = (data) => {
  return axios.post("delete_ticker", data, {
    headers: {
      "Content-Type": "application/json",
    },
  });
};

export const CryptoDeleteTicker = (data) => {
  return axios.post("delete_crypto", data, {
    headers: {
      "Content-Type": "application/json",
    },
  });
};

export const searchTicker = (text) => {
  return axios.get(`search/${text}`, {
    headers: {
      "Content-Type": "application/json",
    },
  });
};

export const searchCrypto = (text) => {
  return axios.get(`searchcrypto/${text}`, {
    headers: {
      "Content-Type": "application/json",
    },
  });
};
