import React, { useState, useEffect, useContext } from "react";
import { useHistory } from "react-router-dom";

import unitContext from "../../../context/context";
import { fetchGraphCryptoData } from "../../../utils";

const colors = [
  "#CD113B",
  "#FFA900",
  "#185ADB",
  "#3A6351",
  "#B4846C",
  "#BF1363",
  "#AFB9C8",
  "#4AA96C",
  "#A35709",
  "#FF8882",
];

const ProjectsCryptoTable = () => {
  const { state } = useContext(unitContext);
  const history = useHistory();
  const [tickers, setTickers] = useState(null);

  useEffect(() => {
    const fetcher = async (mode) => {
      try {
        const res = await fetchGraphCryptoData(null, mode);
        const data = res.data;

        const allTickersValues = [];
        for (const key in data) {
          allTickersValues.push({
            company: key,
            value: Number(
              data[key].reduce((a, b) => a + b.value, 0) / data[key].length
            ).toFixed(2),
          });
        }
        setTickers(allTickersValues.slice(0, 10));
      } catch (error) {
        console.log("Table error = ", error);
      }
    };
    fetcher(state.unit);
  }, [state.unit]);

  return (
    <div className="card">
      <div className="card-body">
        <h6 className="card-title " style={{ textAlign: "left" }}>
          Projects
        </h6>
        <div className="table-responsive">
          <table id="dataTableExample" className="table">
            <thead>
              <tr>
                <th>
                  <p id="myfont">Ticker</p>
                </th>
                <th>
                  <p id="myfont" style={{ color: "yellow", width: "20px" }}>
                    {state.unit}
                  </p>
                </th>
              </tr>
            </thead>
            <tbody>
              {tickers?.map((item, i) => (
                <tr
                  key={i}
                  onClick={() => history.push(`/tickers/${item.company}`)}
                  style={{ cursor: "pointer" }}
                >
                  <td>
                    <span
                      className="badge"
                      style={{
                        background: colors[i],
                        padding: "0.5rem",
                        fontSize: "12px",
                      }}
                    >
                      {item.company}
                    </span>
                  </td>
                  <td>
                    <div className="d-flex align-items-baseline">
                      <p
                        className={
                          item.value > 0 ? "text-success" : "text-danger"
                        }
                      >
                        <span>{Number(item.value * 100).toFixed(0)}</span>
                        {item.value > 0 ? (
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="16"
                            height="16"
                            viewBox="0 0 24 24"
                            fill="none"
                            stroke="currentColor"
                            stroke-width="2"
                            stroke-linecap="round"
                            stroke-linejoin="round"
                          >
                            <path d="M12 19V6M5 12l7-7 7 7" />
                          </svg>
                        ) : (
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="16"
                            height="16"
                            viewBox="0 0 24 24"
                            fill="none"
                            stroke="currentColor"
                            stroke-width="2"
                            stroke-linecap="round"
                            stroke-linejoin="round"
                          >
                            <path d="M12 5v13M5 12l7 7 7-7" />
                          </svg>
                        )}
                      </p>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
};

export default ProjectsCryptoTable;
