import React, { useContext } from "react";

import UnitContext from "../../context/context";

const UnitSelector = () => {
  const { dispatch, state } = useContext(UnitContext);

  return (
    <div
      className="btn-group mb-3 mb-md-0"
      role="group"
      aria-label="Basic example"
    >
      {["HOURLY", "DAILY", "WEEKLY"].map((unit) => (
        <button
          onClick={(e) => {
            e.preventDefault();
            dispatch({ type: unit });
          }}
          key={unit}
          type="button"
          className={`btn btn-outline-primary ${
            state.unit === unit.toLowerCase() && "active"
          }`}
        >
          {unit}
        </button>
      ))}
    </div>
  );
};

export default UnitSelector;
