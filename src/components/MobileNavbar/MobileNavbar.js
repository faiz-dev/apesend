import React from "react";
import { Menu } from "react-feather";

const MobileNavbar = ({ href = null, iTag = null }) => {

    const sidebarToggler = (e) => {
        let sidebarToggler = document.querySelector(".sidebar-toggler")
        sidebarToggler.classList.toggle("active")
        sidebarToggler.classList.toggle("not-active")
        if (window.matchMedia('(max-width: 991px)').matches) {
            e.preventDefault();
            console.log(...document.querySelector("body").classList)
            document.querySelector(".sidebar").classList.toggle('sidebar-folded');
            document.querySelector("body").classList.add('sidebar-open');
        }
    }

    return (
        <>
            {href ?
                <a href="#" onClick={sidebarToggler} className="sidebar-toggler">
                    {/* <i data-feather="menu"></i> */}
                    {iTag ?
                        <i data-feather="menu" />
                        :
                        <Menu />
                    }
                </a>
                :
                <div onClick={sidebarToggler} className="sidebar-toggler">
                    {/* <i data-feather="menu"></i> */}
                    <Menu />
                </div>
            }
        </>
    )
}

export default MobileNavbar;