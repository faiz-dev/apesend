import React, { useEffect } from "react";
import ReactGA from "react-ga";
import "./App.css";
import Home from "./pages/Home";
import Faq from "./pages/faq";
import Privacy from "./pages/Privacy";
import { Route, Switch, BrowserRouter as Router } from "react-router-dom";
import NavBar from "../src/pages/NavBar";
import Login from "./pages/Login";

import Admin from "./pages/Admin";


import Analysis from "./pages/Analysis";
import About from "./pages/about";
import Terms from "./pages/terms";
import NotFound from "./pages/notFound";

import GlobalState from "./context/globalState";
import CryptoHome from "./pages/crypto_home";
import CryptoAnalysis from "./pages/crypto_analysis";
import CryptoAdmin from "./pages/crypto_admin";
import Prediction from "./pages/prediction";
import CryptoPrediction from "./pages/cryptoPrediction";

ReactGA.initialize("UA-206111492-1");

function App() {
  useEffect(() => {
    ReactGA.pageview(window.location.pathname + window.location.search);
  });
  return (
    <GlobalState>
      <div className="App">
        <Router>
          <NavBar />
          <Switch>
            <Route path="/analysis" component={Analysis} />
            <Route path="/faq" component={Faq} />
            <Route path="/privacy" component={Privacy} />
            <Route path="/about" component={About} />
            <Route path="/login" component={Login} />
            <Route path="/admin" component={Admin} />
            <Route path="/crypto_home" component={CryptoHome} />
            <Route path="/prediction" component={Prediction} />
            <Route path="/crypto_admin" component={CryptoAdmin} />
            <Route path="/tickers/:ticker" component={Analysis} />
            <Route path="/termsOfUse" component={Terms} />
            <Route path="/404" component={NotFound} />
            <Route exact path="/" component={Home} />
            <Route exact path="/crypto_analysis" component={CryptoAnalysis} />
            <Route exact path="/cryptoPrediction" component={CryptoPrediction} />
            <Route component={Home} />
          </Switch>
        </Router>
      </div>
    </GlobalState>
  );
}

export default App;
